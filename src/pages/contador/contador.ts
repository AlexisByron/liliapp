import {MemoryProvider} from '../../providers/memory/memory';

import {GeneralProvider} from '../../providers/general/general';
import {CommunicationsProvider} from '../../providers/communications/communications';
import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams, AlertController, ToastController} from 'ionic-angular';
import {EntregaAlimentoProvider} from '../../providers/entrega-alimento/entrega-alimento';
import {MortalidadProvider} from '../../providers/mortalidad/mortalidad'; 
import { LoadingController } from 'ionic-angular';
import {DireccionPage} from '../direccion/direccion';
import 'rxjs/add/operator/timeout';
import {UsuarioProvider} from "../../providers/usuario/usuario";

@IonicPage()
@Component({
    selector: 'page-contador',
    templateUrl: 'contador.html',
})
export class ContadorPage {

    constructor(public loadingCtrl: LoadingController, public navCtrl: NavController, public navParams: NavParams, private alertCtrl: AlertController, public coms: CommunicationsProvider, public general: GeneralProvider, public memory: MemoryProvider, private toastCtrl: ToastController,public alimento:EntregaAlimentoProvider,private mortalidad :MortalidadProvider,public usuario:UsuarioProvider) {}

    public fecha = {
        fechaInicio: null,
        horaInicio: null
      }
    public total;
    public pabellones;
    public pabellon;
   
    public Causales=[];
    public crianza;
    public Mortalidad=[];
    public SectorGuia;
    public id_sector;

    ionViewDidLoad() {
        this.usuario.verificarSesionPrevia().then(() => {
            if(this.usuario.userSession["nombre"]==""){
                // console.log('no hay datos');
                this.navCtrl.setRoot('HomePage').then();
            }else{
                this.general._menu.enable(true,'main-menu');
                this.general.controller = this;
                this.id_sector=(this.usuario.userSession.sector["id"]);
                this.SectorGuia=(this.usuario.userSession.sector["nombre_corto"]);
                // this.obtenerHora();
                this.presentLoadingDefault();
                this.setMortalidad();
            }

        });
    }

    presentLoadingDefault() {
      let loading = this.loadingCtrl.create({
        // spinner: 'hide',
        content: 'Cargando información'
      });
    
      loading.present();

      var id_sector={"id_sector":this.id_sector};
      // console.log(id_sector);


      this.alimento.GetPabellones(id_sector).timeout(30000).subscribe((data) => { // Success

        if(Object.keys(data).length>0){ 
          this.pabellones=data;
            var animal={"animal":this.usuario.userSession.sector["animal"]};
            this.mortalidad.getMortalidadesAsistidas(animal).timeout(30000).subscribe((data) => { // Success
                    if(Object.keys(data).length>0){
                        for (let index = 0; index <  Object.keys(data).length; index++) {
                            var causa={"causa":data[index]["mortalidad"],"id_causa":data[index]["id"],"valor":0,"pabellon":this.pabellon,"fecha":this.fecha.fechaInicio+" "+this.fecha.horaInicio};
                            this.Causales.push(causa);
                        }
                        loading.dismiss();
                    }else{
                        this.showAlert("Error","No se encontraron mortalidades ");
                        loading.dismiss();
                        this.navCtrl.setRoot(DireccionPage);
                    }
                },()=>{
                    this.showAlert("Error","No se logro traer mortalidades");
                    loading.dismiss();
                    this.navCtrl.setRoot(DireccionPage);
                }
            );



        }else{
          this.showAlert("Error","No se encontraron pabellones");
            loading.dismiss();
          this.navCtrl.setRoot(DireccionPage);
        }
      },()=>{
        this.showAlert("Error","No se logro traer pabellones");
          loading.dismiss();
        this.navCtrl.setRoot(DireccionPage);
      }
    );
    }

    setMortalidad(){
        var obj={"causa":"Contador General","id_causa":9,"valor":0,"pabellon":this.pabellon,"fecha":this.fecha.fechaInicio+" "+this.fecha.horaInicio};
        this.Mortalidad.push(obj);
    }


    quitarValorCausa(valor){
        if(this.Causales[valor].valor>0){
        this.Causales[valor].valor--;
        }
        this.obtenerTotal();
      }

    agregarValorCausa(valor){

        this.Causales[valor].valor++;
        this.obtenerTotal();

        // if(this.Causales[valor].comentario=="" ){
        //
        //     const prompt = this.alertCtrl.create();
        //     this.Causales[valor].comentario=null;
        //     prompt.setTitle('Desea ingresar un comentario para causa : '+this.Causales[valor].causa);
        //
        //
        //     prompt.addInput({
        //      type: 'textarea',
        //      name: 'Comentario',
        //      placeholder: 'Ingresar comentario'
        //    });
        //
        //    prompt.addButton({
        //      text: 'No',
        //      handler: data => {
        //         this.Causales[valor].comentario=null;
        //
        //      }
        //    });
        //
        //    prompt.addButton({
        //      text: 'Registrar',
        //      handler: data => {
        //          if(data.Comentario==""){
        //              this.Causales[valor].comentario=null;
        //          }else{
        //              this.Causales[valor].comentario=data.Comentario;
        //          }
        //      }
        //    });
        //      prompt.present();
        //    }
    }

    obtenerTotal(){
        this.total=0;
        this.Causales.forEach(element => {
        this.total=this.total+Number(element.valor);
        });
        this.total=this.total+Number(this.Mortalidad[0].valor);
      }

    CambioPabellon(){
        this.Causales.forEach(element => {
            element.pabellon=this.pabellon;
            element.valor=0;
        });
        // console.log(this.Causales);
        this.Mortalidad[0].pabellon=this.pabellon;
        this.Mortalidad[0].valor=0;
        this.obtenerTotal();

      }

    mortalidadAdd(){
        this.Mortalidad[0].valor++;

        // if(this.Mortalidad[0].comentario=="" ){
        //
        //     const prompt = this.alertCtrl.create();
        //     this.Mortalidad[0].comentario=null;
        //     prompt.setTitle('Desea ingresar un comentario para causa N.A: ');
        //
        //
        //     prompt.addInput({
        //      type: 'textarea',
        //      name: 'Comentario',
        //      placeholder: 'Ingresar comentario'
        //    });
        //
        //    prompt.addButton({
        //      text: 'No',
        //      handler: data => {
        //        this.Mortalidad[0].comentario=null;
        //      }
        //    });
        //
        //    prompt.addButton({
        //      text: 'Registrar',
        //      handler: data => {
        //          if (data.Comentario==""){
        //              this.Mortalidad[0].comentario=null;
        //          } else{
        //              this.Mortalidad[0].comentario=data.Comentario;
        //          }
        //
        //
        //      }
        //    });
        //      prompt.present();
        //    }

        this.obtenerTotal();
    }

    mortalidadMenos(){
        if(this.Mortalidad[0].valor>0){
            this.Mortalidad[0].valor--;
            this.obtenerTotal();
        }
    }

    // obtenerHora() {
    //     let fecha = new Date(Date.now());
    //     let hora = new Date(Date.now());
    //     let month = (fecha.getMonth() + 1) + '';
    //     let minutos = (hora.getMinutes() + '');
    //
    //     month = month.length == 1 ? '0' + month : month;
    //
    //     let day = fecha.getDate() + '';
    //     day = day.length == 1 ? '0' + day : day;
    //     minutos = minutos.length == 1 ? '0' + minutos : minutos;
    //
    //     this.fecha.fechaInicio = fecha.getFullYear() + '-' + month + '-' + day;
    //
    //     if(hora.getHours().toString().length==1){
    //         this.fecha.horaInicio = "0"+hora.getHours() + ':' + minutos;
    //     }else{
    //         this.fecha.horaInicio = hora.getHours() + ':' + minutos;
    //     }
    //
    // }

    Eviar(){
        // console.log(this.id_sector);
        let error = false;
        let mostrar_envio_success = true;

        this.Causales.forEach(element => {
            (element.fecha=this.fecha.fechaInicio+" "+this.fecha.horaInicio);

            //console.log(element);

            if(element.valor>0){
                  this.mortalidad.InsertMortalidad(element).timeout(30000).subscribe((data) => { // Success cambiar por mortalidad

                    if(mostrar_envio_success) {
                      this.showAlert("Exito","Registros agregado");
                      this.Causales.forEach(element => {
                        element.valor=0;
                    });
                    this.Mortalidad[0].valor=0;
                    this.pabellon=null;
                    this.total=0;
                    this.fecha.fechaInicio=null;
                    this.fecha.horaInicio=null;
                    // this.obtenerHora();
                    }
                    mostrar_envio_success = !mostrar_envio_success
                    
  

                },()=>{
                  
                  if(!error){
                    this.showAlert("Error ","No se logro agrear registros de  mortalidad");
                  }
                  error=true;
                });
              }
          });


          
         

          if(this.Mortalidad[0].valor>0){ // inserta contador general
            this.Mortalidad[0].fecha=this.fecha.fechaInicio+" "+this.fecha.horaInicio;
            
                this.mortalidad.InsertMortalidad(this.Mortalidad[0]).timeout(30000).subscribe((data) => { // Success

                  if(mostrar_envio_success) {
                    this.showAlert("Exito","Registros agregado");
                    this.Causales.forEach(element => {
                      element.valor=0;
                  });
                  this.Mortalidad[0].valor=0;
                  this.pabellon=null;
                  this.total=0;
                  this.fecha.fechaInicio=null;
                  this.fecha.horaInicio=null;
                  }
                  mostrar_envio_success = !mostrar_envio_success
                  // this.obtenerHora();

              },()=>{
                if(!error){
                  this.showAlert("Error ","No se logro agrear registros de  mortalidad");
                }
                error=true;
              });

          }

       
          
    }

    showAlert(titulo,mensaje) {
        const alert = this.alertCtrl.create({
          title: titulo,
          subTitle: mensaje,
          buttons: ['OK']
        });
        alert.present();
      }

}
