import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ConfirmacionContadorPage } from './confirmacion-contador';

@NgModule({
  declarations: [
    ConfirmacionContadorPage,
  ],
  imports: [
    IonicPageModule.forChild(ConfirmacionContadorPage),
  ],
})
export class ConfirmacionContadorPageModule {}
