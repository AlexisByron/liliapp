import { HomePage } from './../home/home';
import { MemoryProvider } from './../../providers/memory/memory';
import { CommunicationsProvider } from './../../providers/communications/communications';
import { GeneralProvider } from './../../providers/general/general';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the ConfirmacionContadorPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-confirmacion-contador',
  templateUrl: 'confirmacion-contador.html',
})
export class ConfirmacionContadorPage {

  constructor(public navCtrl: NavController, public navParams: NavParams, public general: GeneralProvider, public coms: CommunicationsProvider, public memory: MemoryProvider) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ConfirmacionContadorPage');
  }

  
  logout(){
    this.general.presentPrompt('Cerrar sesión','¿Esta seguro que desea cerrar sesión?','Si','No').then(()=>{


      this.coms.logout(this.memory.userToken)

      this.memory.token = null;
      this.memory.user = null;
      this.general.controller.navCtrl.setRoot(HomePage);
    })
  }

}
