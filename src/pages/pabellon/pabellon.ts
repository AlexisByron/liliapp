import { CommunicationsProvider } from './../../providers/communications/communications';
import { GeneralProvider } from './../../providers/general/general';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { MemoryProvider } from '../../providers/memory/memory';
import {MortalidadProvider} from '../../providers/mortalidad/mortalidad'; 
import { AlertController } from 'ionic-angular';
import {DireccionPage} from '../direccion/direccion';
import { LoadingController } from 'ionic-angular'
import 'rxjs/add/operator/timeout';
import {UsuarioProvider} from '../../providers/usuario/usuario';
import {MainviewPage} from '../mainview/mainview';

@IonicPage()
@Component({
  selector: 'page-pabellon',
  templateUrl: 'pabellon.html',
})
export class PabellonPage {


  public userProfile = null;

  constructor(public loadingCtrl: LoadingController,public navCtrl: NavController, public navParams: NavParams, 
    public memory : MemoryProvider, public general : GeneralProvider,
     private coms : CommunicationsProvider,private mortalidad :MortalidadProvider,public alertCtrl: AlertController,public usuario:UsuarioProvider) {

  }

  public causales=[];
  public total=0;
  public sector="";
  public pabellon=0;
  public crianza=0;
  public id_sector;
  public id_pabellon=0;

  public fecha = {
    fechaInicio: null,
    horaInicio: null
  }

  ionViewDidLoad(){
      if(this.navParams.get("datos")==undefined){
          this.navCtrl.setRoot('MainviewPage').then();
      }else{
          this.sector=this.navParams.get("datos")["sector"];
          this.id_sector =this.navParams.get("datos")["id_sector"];

          this.fecha.fechaInicio=this.navParams.get("datos")["fecha"]["fechaInicio"];
          this.fecha.horaInicio=this.navParams.get("datos")["fecha"]["horaInicio"];
          this.pabellon=this.navParams.get("datos")["pabellon"];
          this.crianza=this.navParams.get("datos")["nro_crianza"];
          this.id_pabellon=this.navParams.get("datos")["id_pabellon"];

       
          this.presentLoadingDefault();
      }


  }



  presentLoadingDefault() {
    let loading = this.loadingCtrl.create({
      // spinner: 'hide',
      content: 'Cargando información'
    });
  
    loading.present();

 

    var animal={"animal":this.usuario.userSession.sector["animal"]};
     this.mortalidad.getMortalidades(animal).timeout(30000).subscribe((data) => { // Success
      if(Object.keys(data).length>0){
        for (let index = 0; index <  Object.keys(data).length; index++) {
          var causa={"causa":data[index]["mortalidad"],"id_causa":data[index]["id"],"valor":0,"pabellon":this.id_pabellon,"fecha":this.fecha.fechaInicio+" "+this.fecha.horaInicio};
        this.causales.push(causa);
       
     }
     loading.dismiss();
      }else{
        this.showAlert("Error","No se encontraron mortalidades");
        //this.navCtrl.setRoot(DireccionPage);
      }
     
      },() => {
        this.showAlert("Error","No se logro traer causas");
        
      }
    ); 

  }


    agregarValorCausa(valor){

        this.causales[valor].valor++;
        // console.log();
        // if(this.causales[valor].comentario=="" ){
        //
        //     const prompt = this.alertCtrl.create();
        //
        //     prompt.setTitle('Desea ingresar un comentario para causa: ');
        //     prompt.setSubTitle(this.causales[valor].causa);
        //
        //     prompt.addInput({
        //         type: 'textarea',
        //         name: 'Comentario',
        //         placeholder: 'Ingresar comentario'
        //     });
        //
        //     prompt.addButton({
        //         text: 'No',
        //         handler: data => {
        //             this.causales[valor].comentario=null;
        //
        //         }
        //     });
        //
        //     prompt.addButton({
        //         text: 'Registrar',
        //         handler: data => {
        //             this.causales[valor].comentario=data.Comentario;
        //             // console.log(data);
        //         }
        //     });
        //
        //
        //
        //     prompt.present();
        // }

        this.obtenerTotal();
    }

    cambioValor(i){
        // console.log(this.causales[i]);
    }

    quitarValorCausa(valor){
        if(this.causales[valor].valor>0){
            this.causales[valor].valor--;
        }
        this.obtenerTotal();
    }

    obtenerTotal(){

        this.total=0;
        this.causales.forEach(element => {

            this.total=this.total+Number(element.valor);
        });

    }

     guardar(){

         var a=0;
         //var error="";

         let error = false;
        let mostrar_envio_success = true;
        
         this.causales.forEach(element => {
    
             (element.fecha=this.fecha.fechaInicio+" "+this.fecha.horaInicio);

         
             if(element.valor>0){
                 // console.log(element)
                 this.mortalidad.InsertMortalidad(element).timeout(30000).subscribe((data) => { // Success
                  if(mostrar_envio_success) {
                    this.showAlert("Exito","Registro agregado");
                    this.navCtrl.setRoot(MainviewPage);
                  }

                  mostrar_envio_success = !mostrar_envio_success
                  
                 },()=>{
                  if(!error){
                    this.showAlert("Error ","No se logro agrear registros de  mortalidad");
                  }
                  error=true;
                 });
    
             }
         });
    
         /*if(error==""){
             this.showAlert("Exito","Registro agregado");
             this.navCtrl.setRoot(MainviewPage);
          
         }else{
             this.showAlert("Error",error);
         }*/
    
     }
     

    showAlert(titulo,mensaje) {
        const alert = this.alertCtrl.create({
            title: titulo,
            subTitle: mensaje,
            buttons: ['OK']
        });
        alert.present();
    }

}
