import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PabellonPage } from './pabellon';

@NgModule({
  declarations: [
    PabellonPage,
  ],
  imports: [
    IonicPageModule.forChild(PabellonPage),
  ],
})
export class PabellonPageModule {}
