import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MainviewPage } from './mainview';

@NgModule({
  declarations: [
    MainviewPage,
  ],
  imports: [
    IonicPageModule.forChild(MainviewPage),
  ],
})
export class MainviewPageModule {}
