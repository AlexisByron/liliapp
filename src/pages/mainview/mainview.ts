import {GeneralProvider} from '../../providers/general/general';
import {Component, ViewChild, ElementRef} from '@angular/core';
import {AlertController, IonicPage, NavController, NavParams} from 'ionic-angular';
import {MemoryProvider} from '../../providers/memory/memory';
import * as moment from 'moment';
import {CommunicationsProvider} from '../../providers/communications/communications';
//import { IBeacon } from '@ionic-native/ibeacon';
import {Platform, DateTime} from 'ionic-angular';
import {HomePage} from '../home/home';
import {EntregaAlimentoProvider} from '../../providers/entrega-alimento/entrega-alimento';
import {MortalidadProvider} from '../../providers/mortalidad/mortalidad';
import { LoadingController } from 'ionic-angular'
import {DireccionPage} from '../direccion/direccion';
import {UsuarioProvider} from "../../providers/usuario/usuario";
import {CrianzaProvider} from '../../providers/crianza/crianza' 

@IonicPage()
@Component({
    selector: 'page-mainview',
    templateUrl: 'mainview.html',
})
export class MainviewPage {

    //@ViewChild('dateinput') dateInput

    @ViewChild('dateinput') timePicker: DateTime;
    constructor(public loadingCtrl: LoadingController,public navCtrl: NavController, public navParams: NavParams, public memory: MemoryProvider, private coms: CommunicationsProvider, public general: GeneralProvider/* , private ibeacon: IBeacon */, private alertCtrl: AlertController,public alimento:EntregaAlimentoProvider,public mortalidad:MortalidadProvider,public usuario:UsuarioProvider,public crianza :CrianzaProvider) {
        
    }
    public origen;
    public pabellones;
    public cantidadP;//cantidad pabellones segun sector
    public userProfile = null;
    public id_crianza;
    public fecha = {
        fechaInicio: null,
        horaInicio: null
      }

      



    public SectorGuia;
    public id_sector;


    ionViewDidLoad() {

        this.usuario.verificarSesionPrevia().then(() => {
            if(this.usuario.userSession["nombre"]==""){
                // console.log('no hay datos');
                this.ToHome();
            }else{
                this.general._menu.enable(true,'main-menu');
                this.general.controller = this;
                this.id_sector=(this.usuario.userSession.sector["id"]);
                this.SectorGuia=(this.usuario.userSession.sector["nombre_corto"]);

                // this.obtenerHora();
                this.presentLoadingDefault();
            }

        });



        
    }

    ToHome(){
        this.navCtrl.setRoot(HomePage);
    }

    presentLoadingDefault() {
        let loading = this.loadingCtrl.create({
          // spinner: 'hide',
          content: 'Cargando información'
        });
      
        loading.present();

        var id_sector={"id_sector":this.id_sector};

        this.alimento.GetPabellones(id_sector).subscribe((data) => { // Success
            // console.log(data);
            if(Object.keys(data).length>0){
                this.pabellones=data;
                this.cantidadP=(this.pabellones.length);

                this.crianza.GetCrianza(id_sector).timeout(30000).subscribe((data) => { // Success
                  // console.log(data[0]);
                  this.id_crianza=data[0]["nro_crianza"];
                },()=>{
                  // console.log("problema al traer crianza");
                });
                loading.dismiss();

            }else{
                this.showAlert("Error","No existe sector");
                 this.navCtrl.setRoot(DireccionPage);
            }

        });
      }

    // obtenerHora() {
    //     let fecha = new Date(Date.now());
    //     let hora = new Date(Date.now());
    //     let month = (fecha.getMonth() + 1) + '';
    //     let minutos = (hora.getMinutes() + '');
    //
    //     month = month.length == 1 ? '0' + month : month;
    //
    //     let day = fecha.getDate() + '';
    //     day = day.length == 1 ? '0' + day : day;
    //     minutos = minutos.length == 1 ? '0' + minutos : minutos;
    //
    //     this.fecha.fechaInicio = fecha.getFullYear() + '-' + month + '-' + day;
    //
    //     if(hora.getHours().toString().length==1){
    //         this.fecha.horaInicio = "0"+hora.getHours() + ':' + minutos;
    //     }else{
    //         this.fecha.horaInicio = hora.getHours() + ':' + minutos;
    //     }
    //
    // }

    nuevoPabellon() {

     

        if(this.fecha.fechaInicio!=null && this.fecha.horaInicio!=null){
            let alert = this.alertCtrl.create();
            alert.setTitle('Seleccione el número de pabellón');

            this.pabellones.forEach(element => {
                alert.addInput({
                    type: 'radio',
                    label: element.nro_pabellon,
                    value: element.nro_pabellon,
                    checked: false
                });
            });

          

            alert.addButton('Cancelar');

            alert.addButton({
                text: 'Ingresar',
                handler: data => {
                    
                    if (data){
                        let id_pab;
                        this.pabellones.forEach(element => {
                            if(element.nro_pabellon==data){
                              id_pab=element.id
                            }
                          });
                         

                        var datos={"sector":this.SectorGuia,"id_sector":this.id_sector,"pabellon":data,"nro_crianza":this.id_crianza,"fecha":this.fecha,"id_pabellon":id_pab};
                        //console.log(datos);
                        this.navCtrl.push('PabellonPage',{
                            datos:datos
                        });

                    }
                }
            });
            alert.present();
        }else{
            this.showAlert("Error","Debe ingresar fecha y hora");
        }

        
    }

    showAlert(titulo,mensaje) {
      const alert = this.alertCtrl.create({
        title: titulo,
        subTitle: mensaje,
        buttons: ['OK']
      });
      alert.present();
    }
}
