import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { MemoryProvider } from './../../providers/memory/memory';
import { GeneralProvider } from './../../providers/general/general';
import { CommunicationsProvider } from './../../providers/communications/communications';
import {EntregaAlimentoProvider} from '../../providers/entrega-alimento/entrega-alimento';
import { AlertController } from 'ionic-angular';
import { LoadingController } from 'ionic-angular';
import {DireccionPage} from '../direccion/direccion';
import 'rxjs/add/operator/timeout';
import {UsuarioProvider} from "../../providers/usuario/usuario";
import {HomePage} from "../home/home";
@IonicPage()
@Component({
  selector: 'page-fromulario-carga-alimento',
  templateUrl: 'fromulario-carga-alimento.html',
})
export class FromularioCargaAlimentoPage {

  constructor(public loadingCtrl: LoadingController,public navCtrl: NavController, public navParams: NavParams, public memory: MemoryProvider, public coms: CommunicationsProvider, public general: GeneralProvider,public alimento :EntregaAlimentoProvider,public alertCtrl: AlertController,public usuario:UsuarioProvider) {
  }

  public fecha = {
    fechaInicio: null,
    fechaInicioCrianza: null,
    horaInicio: null
  }

  //variabnles
  //variables generales

  public NumeroGuia;
  public SectorGuia;
  public PabellonesGuia;

 

  public ListaDietaGuia;
  public ListaPabellones:any=[];


  public id_silo_1;
  public id_silo_2;
  //variables de la descarga

  public Pabellon;
  public KilosSilo1;
  public DietaSilo1;
  public KilosSilo2;
  public DietaSilo2;

  public id_sector;

  ionViewDidLoad(){

    this.usuario.verificarSesionPrevia().then(() => {
      if(this.usuario.userSession["nombre"]==""){
        // console.log('no hay datos');
        this.ToHome();
      }else{
        this.general._menu.enable(true,'main-menu');
        this.general.controller = this;
        this.id_sector=(this.usuario.userSession.sector["id"]);
        this.SectorGuia=(this.usuario.userSession.sector["nombre_corto"]);
        // this.obtenerHora();
        this.presentLoadingDefault();
      }

    });
    

  }

  ToHome(){
    this.navCtrl.setRoot(HomePage);
  }

  presentLoadingDefault() {
    let loading = this.loadingCtrl.create({
      // spinner: 'hide',
      content: 'Cargando información'
    });

    loading.present();

    var id ={"id":this.id_sector};
    // console.log(id);

    var id_sector={"id_sector":this.id_sector};
    this.alimento.GetPabellones(id_sector).timeout(30000).subscribe((data) => { // Success
      // console.log(data);
      if(Object.keys(data).length>0){
        this.ListaPabellones=(data);

        loading.dismiss();
      }else{
        this.showAlert("Error","no se encontraron pabellones");
        this.navCtrl.setRoot(DireccionPage);
      }

    }, () => {
      this.showAlert("Error", "no se logro traer información, intentar nuevamente");
    });

  }


  getIdSilos(){
    var id ={"id_pabellon":this.Pabellon};
    //console.log(id);
    this.alimento.GetIdSilo(id).timeout(30000).subscribe((data) => { // Success
       //console.log(data);
      if(Object.keys(data).length>0){
        this.id_silo_1=data[0]["id"];
        this.id_silo_2=data[1]["id"];
      }else{
        this.showAlert("Error"," no se encontraron silos");
        this.navCtrl.setRoot(DireccionPage);
      }

    },()=>{
      this.showAlert("Error"," no se logro traer silos");
      this.navCtrl.setRoot(DireccionPage);
    }
  );
  }


  // obtenerHora() {
  //   let fecha = new Date(Date.now());
  //   let hora = new Date(Date.now());
  //   let month = (fecha.getMonth() + 1) + '';
  //   let minutos = (hora.getMinutes() + '');
  //
  //   month = month.length == 1 ? '0' + month : month;
  //
  //   let day = fecha.getDate() + '';
  //   day = day.length == 1 ? '0' + day : day;
  //   minutos = minutos.length == 1 ? '0' + minutos : minutos;
  //
  //   this.fecha.fechaInicio = fecha.getFullYear() + '-' + month + '-' + day;
  //
  //   if(hora.getHours().toString().length==1){
  //     this.fecha.horaInicio = "0"+hora.getHours() + ':' + minutos;
  //   }else{
  //     this.fecha.horaInicio = hora.getHours() + ':' + minutos;
  //   }
  //
  // }


  validarNumeroGuia(){
    if(this.NumeroGuia.trim().length==0){
      this.SectorGuia=null;
      this.showAlert("error","Completa el numero guía");
    }else{
      let loading = this.loadingCtrl.create({
        // spinner: 'hide',
        content: 'Cargando informacion'
      });
    
      loading.present();
  
      var guia ={"nro_guia":this.NumeroGuia};
      this.alimento.GetGuias(guia).timeout(30000).subscribe((data) => { // Success
        // console.log(Object.keys(data).length);
        if(Object.keys(data).length>0){
          // console.log(data[0]["dieta_id"]);
           this.DietaSilo1=data[0]["dieta_id"];
            this.DietaSilo2=data[0]["dieta_id"];
            this.ListaDietaGuia=data[0]["dieta"].toString();
            loading.dismiss();
        }else{
          loading.dismiss();
          this.showAlert("Error","El numero de guía ingresado no se encuentra registrado");
          this.DietaSilo1="";
            this.DietaSilo2="";
            this.ListaDietaGuia="";
        }
        
      },()=>{
        this.showAlert("Error","No se puede validar la guia en este momento");
        this.navCtrl.setRoot(DireccionPage);
      }

    );
  
    }//
  

  }


  Agregar(){

    var guia ={"nro_guia":this.NumeroGuia};
    
    var error=false;
    this.alimento.GetGuias(guia).timeout(30000).subscribe((data) => { // Success
      //console.log(Object.keys(data).length);
     
      if(Object.keys(data).length==0){
       this.showAlert("Error","El numero de  guía de despacho no existe, primero debe se agregada");
      }else{
        var objeto ={"NumeroGuia":this.NumeroGuia,"SectorGuia":this.id_sector,"DietaSilo":this.DietaSilo1,"Pabellon":this.Pabellon,"kilos_silo":this.KilosSilo1,"silo":this.id_silo_1,"fecha":this.fecha.fechaInicio+" "+this.fecha.horaInicio};
        var objeto2 ={"NumeroGuia":this.NumeroGuia,"SectorGuia":this.id_sector,"DietaSilo":this.DietaSilo2,"Pabellon":this.Pabellon,"kilos_silo":this.KilosSilo2,"silo":this.id_silo_2,"fecha":this.fecha.fechaInicio+" "+this.fecha.horaInicio};
    
  
      // console.log(objeto);
      // console.log(objeto2);
     

        if(this.KilosSilo1>0 || this.KilosSilo2>0){

          if(this.KilosSilo1>0 && this.DietaSilo1!=""){
            // console.log("se envia silo 1 " + this.KilosSilo1)
            this.alimento.AgregarDescargaAlimento(objeto).timeout(30000).subscribe((data) => { // Success
                  console.log(data);

                  if(error==false){
                    this.showAlert("Exito","se agrego la dieta al silo 1");
                           this.NumeroGuia=null;
                           this.KilosSilo1=null;
                           this.KilosSilo2=null;
                           this.DietaSilo1=null;
                           this.DietaSilo2=null;
                           this.Pabellon=null;
                           this.ListaDietaGuia=null;
                           this.fecha.fechaInicio=null;
                           this.fecha.horaInicio=null;
                           // this.obtenerHora();
                  }

                 },()=>{
                   this.showAlert("Error","No se logro agregar carga de elimento ,silo 1 ,intenar nuevamente");
                   error=true;
                 }
              );

          }

          if(this.KilosSilo2>0 && this.DietaSilo2!=""){
            // console.log("se envia silo 2 " + this.KilosSilo2)
            this.alimento.AgregarDescargaAlimento(objeto2).timeout(30000).subscribe((data) => { // Success
                  console.log(data);

                  if(error==false){
                    this.showAlert("Exito","se agrego la dieta al silo 2");
                           this.NumeroGuia=null;
                           this.KilosSilo1=null;
                           this.KilosSilo2=null;
                           this.DietaSilo1=null;
                           this.DietaSilo2=null;
                           this.Pabellon=null;
                           this.ListaDietaGuia=null;
                           this.fecha.fechaInicio=null;
                           this.fecha.horaInicio=null;
                           // this.obtenerHora();
                  }

                 },()=>{
                  this.showAlert("Error","No se logro agregar carga de elimento ,silo 2 , intentar nuevamente");
                  error=true;
                }
              );
          }

         

        }else{
          this.showAlert("Error","debe ingresar kilos en almenos un silo");
        }

       // console.log(error);
      
      }
    },()=>{
      this.showAlert("Error","No fue posible ejecutar la accion")
    }
  );
    console.log(error);
  }


  showAlert(titulo,mensaje) {
    const alert = this.alertCtrl.create({
      title: titulo,
      subTitle: mensaje,
      buttons: ['OK']
    });
    alert.present();
  }


}
