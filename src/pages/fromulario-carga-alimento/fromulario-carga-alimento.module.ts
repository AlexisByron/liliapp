import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FromularioCargaAlimentoPage } from './fromulario-carga-alimento';

@NgModule({
  declarations: [
    FromularioCargaAlimentoPage,
  ],
  imports: [
    IonicPageModule.forChild(FromularioCargaAlimentoPage),
  ],
})
export class FromularioCargaAlimentoPageModule {}
