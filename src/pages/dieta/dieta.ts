import { GeneralProvider } from './../../providers/general/general';
import { MemoryProvider } from './../../providers/memory/memory';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { CommunicationsProvider } from '../../providers/communications/communications';

/**
 * Generated class for the DietaPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-dieta',
  templateUrl: 'dieta.html',
})
export class DietaPage {

  constructor(public navCtrl: NavController, public navParams: NavParams, public coms: CommunicationsProvider, public memory: MemoryProvider, public general: GeneralProvider,
  public toastCtrl: ToastController) {
  }


  private animales = null;
  private origenes = null;
  private pabellones = [];
  private usuario = null;
  public sectores = null;
  public pabellonesAuth = [];
  public userProfile = null;
  public crianza = null;
  
  public informacion = {

    sector: null,
    idPabellon: null,
    fechaInicio: null,
    numeroGuia: null,
    dieta: null,
    silos: null

 
 
    


  }

  public fecha = {
    fechaInicio: null,
    fechaInicioCrianza: null,
    horaInicio: null
  }

  public dietas = ["Broiler Prestarter CB", "Broiler Prestarter LG", "Broiler Prestarter LM", "Broiler Inicial CB", "Broiler Inicial LG", "Broiler Inicial LM", "Broiler Mediana CB", "Broiler Mediana LG", "Broiler Mediana LM", "Broiler Final CB", "Broiler Final LG", "Broiler Final LM"]

  

  ionViewDidLoad(){
    this.coms.getObject(this.memory.userToken).then(data => {
      console.log("data de mongo", data);
      
      this.sectores = data['data']; 
    
        console.log(this.sectores);
    
       
      
     /*  for(let i = 0; i < this.sectores.length; i++) {
        for(let j = 0; j < this.sectores[i].pabellones.length; j++) {
          this.pabellones.push(this.sectores[i].pabellones[j]);
        }
      } */
    
    
       
       // mapear por sectores para guardar pabellones y reflejarlos en el HTML
     
       
    });
    this.general._menu.enable(true,'main-menu');
    this.general.controller = this;
    let fecha = new Date(Date.now());
    this.informacion.fechaInicio = fecha;
    
    let hora = new Date(Date.now());
    let month = (fecha.getMonth()+1)+'';
    let minutos = (hora.getMinutes()+'');
 
    month = month.length == 1 ? '0'+month : month;
 
    let day = fecha.getDate() + '';
    day =  day.length == 1 ? '0'+day : day;
    minutos = minutos.length == 1 ? '0'+minutos : minutos;
 
    this.fecha.fechaInicio = fecha.getFullYear() + '-' + month + '-' + day;
    this.fecha.fechaInicioCrianza = fecha.getFullYear() + '-' + month + '-' + day;
    this.fecha.horaInicio = hora.getHours() + ':' + minutos;
    console.log("user profile", this.memory.userProfile);
    this.userProfile = this.memory.userProfile;
    /* this.controlHora = hora. */
    
    


 
  }

  ionViewWillEnter(){

 /*    public causales= ['Muerte Súbita','Chicos','Problemas Patas','Coccidiosis','Ascitis','Hepatitis Viral', 'Vomito Negro', 'Reteción Vitelo', 'Colibacilosis', "Alteración-Riñones", 'Endocarditis', 'Transporte', 'Otros']
    public causalesCerdos = ['Alergia', 'Asfixia', 'Atresia Anal', 'Bajo Desarrollo', 'Canibalismo', 'Cargado Faenadora', 'Cojera', 'Diarrea Severa', 'Endocarditis Valvular', 'Epidermitis', 'Hemorragia Interna', 'Hernia Inguinal', 'Hernia Umbilical', 'Ileitis', 'Lesion Inguinal', 'Miscelaneos', 'Muestra Laboratorio', 'Neumonía', 'Neumonía Atípica', 'No Viable(1 a 3 días)', 'Pericarditis', 'Peritonitis', 'Poliserositis', 'Robo', 'Ruptura Hepática', 'Asistencia', 'Septicemia', 'Sindrome de Insuficiencia Cardiaca', 'Sindrome de Estrés Porcino', 'Torsión Intestinal', 'Transporte', 'Úlcera Gástrica', 'Úlcera Crónica'];
    public pab = {
      pabellon: 1,
      causales: [
        0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
      ]
    }
  
    public causas= [ "0","0","0","0","0","0","0","0", "0","0","0","0","0","0","0","0","0","0","0","0","0", "0","0","0","0","0","0", "0","0","0","0","0"]
  
    this.pab = {
      pabellon: 1,
      causales: [
        0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
      ]
    }
  
    this.causas= [ "0","0","0","0","0","0","0","0", "0","0","0","0","0","0","0","0","0","0","0","0","0", "0","0","0","0","0","0", "0","0","0","0","0"]; */
    this.coms.getDietaInfo(this.memory.userToken).then(data => {

  
  
      console.log(this.pabellones);
 /*      for(let i = 1; i < (data['pabellones'][0] + 1); i++) {
          this.pabellones.push(i);
          console.log(this.pabellones);
      } */
      
     
    
       
    
       
      
     /*  for(let i = 0; i < this.sectores.length; i++) {
        for(let j = 0; j < this.sectores[i].pabellones.length; j++) {
          this.pabellones.push(this.sectores[i].pabellones[j]);
        }
      } */
    
    
       
       // mapear por sectores para guardar pabellones y reflejarlos en el HTML
     
       
    });

  }



 
/*   submit = data => {
    console.log(this.informacion);
   
    this.coms.postCrianza(this.informacion, this.memory.userToken).then(() => {
      this.presentToast();
    
      this.informacion = {
        sector: null,
        idPabellon: null,
        fechaInicio: null,
        numeroGuia: null,
        dieta: null,
        silos: null
    
      }

      this.navCtrl.push('ConfirmacionFormularioPage')
    }).catch(() => {
      this.presentError();
    }
     
    );
    
  } */
  submit = () => {
    this.coms.postDieta(this.informacion, this.memory.userToken).then((data) => {
      console.log(this.informacion);
    }).catch((err) => {
      console.log(err);
    }
     
    );
  }
  onFilter = (sector) => {
    this.pabellonesAuth = []
    let counter = 1;
    for(let i = 0; i< sector.capacidadPabellones.length; i++) {
      this.pabellonesAuth.push(sector.capacidadPabellones[i])
    }
    
    this.pabellones = this.pabellonesAuth.map(pabellon => {
     
      return counter++;
    });
  /*   this.crianzas.forEach( crianza => {
      if (this.userProfile['sector'] === crianza['sector']) {
        console.log("aqui la crianza del sector", crianza['sector'])
      }
    }); */

    for(let i = 0; i< this.sectores.length; i++) {
      if (this.userProfile['sector'] === this.sectores[i]['nombre']) {
        this.crianza = this.sectores[i]['crianzaActual'];
        console.log(this.crianza);
      }
      console.log(this.userProfile['sector'], this.sectores[i]);
    }

    console.log( "user profile en filter",this.userProfile);
   
    
  }; 

  presentToast() {
    let toast = this.toastCtrl.create({
      message: 'Su formulario se ha enviado con éxito',
      duration: 3000,
      position: 'top'
    });
  
    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });
  
    toast.present();
  }

  presentError() {
    let toast = this.toastCtrl.create({
      message: 'Error de conexion',
      duration: 3000,
      position: 'top'
    });
  
    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });
  
    toast.present();
  }

}
