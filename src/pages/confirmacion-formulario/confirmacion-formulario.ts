import { HomePage } from './../home/home';
import { MemoryProvider } from './../../providers/memory/memory';
import { GeneralProvider } from './../../providers/general/general';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { CommunicationsProvider } from '../../providers/communications/communications';

/**
 * Generated class for the ConfirmacionFormularioPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-confirmacion-formulario',
  templateUrl: 'confirmacion-formulario.html',
})
export class ConfirmacionFormularioPage {

  constructor(public navCtrl: NavController, public navParams: NavParams, public coms: CommunicationsProvider, public general: GeneralProvider, public memory: MemoryProvider) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ConfirmacionFormularioPage');

  
  }

  logout(){
    this.general.presentPrompt('Cerrar sesión','¿Esta seguro que desea cerrar sesión?','Si','No').then(()=>{
  
  
      this.coms.logout(this.memory.userToken)
  
      this.memory.token = null;
      this.memory.user = null;
      this.general.controller.navCtrl.setRoot(HomePage);
    })
  }

}

  
