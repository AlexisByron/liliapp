import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ConfirmacionFormularioPage } from './confirmacion-formulario';

@NgModule({
  declarations: [
    ConfirmacionFormularioPage,
  ],
  imports: [
    IonicPageModule.forChild(ConfirmacionFormularioPage),
  ],
})
export class ConfirmacionFormularioPageModule {}
