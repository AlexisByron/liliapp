import { MemoryProvider } from './../../providers/memory/memory';
import { GeneralProvider } from './../../providers/general/general';
import { Component } from '@angular/core';
import {IonicPage, MenuController, NavController, NavParams,AlertController} from 'ionic-angular';
import {UsuarioProvider} from '../../providers/usuario/usuario';
import {ScreenOrientation} from "@ionic-native/screen-orientation";
import {HomePage} from "../home/home";

import {Storage} from "@ionic/storage";
import {MainviewPage} from "../mainview/mainview";

@IonicPage()
@Component({
  selector: 'page-direccion',
  templateUrl: 'direccion.html',
})
export class DireccionPage {

  public formulario = null;
  public permiso  = null;
  public sector = null;
  public page="";

  constructor(public navCtrl: NavController, public navParams: NavParams, public general: GeneralProvider, public memory: MemoryProvider,public usuario:UsuarioProvider, public menuCtrl: MenuController,public alert:AlertController) {
    // navCtrl.setRoot(HomePage);
  }



  ionViewDidLoad() {

    this.usuario.TestCon().timeout(5000).subscribe((data) => { // Success
      //console.log("hola");
     },()=>{
      this.showAlert("Error","Usted no tiene conexión a internet");
    }
  );

   
    this.usuario.verificarSesionPrevia().then(() => {
      if(this.usuario.userSession["nombre"]==""){
        this.ToHome();

      }else{
        this.general._menu.enable(true,'main-menu');
        this.general.controller = this;
        this.sector=(this.usuario.userSession.sector["nombre_corto"]);
        this.permiso=(this.usuario.userSession.sector["animal"]);
        // console.log(this.permiso);
      }

    })

    this.general.mostrarSelectorFormulario = false;
  }


  showAlert(titulo,mensaje) {
    const alert = this.alert.create({
      title: titulo,
      subTitle: mensaje,
      buttons: ['OK']
    });
    alert.present();
  }


  ToHome(){
    this.navCtrl.setRoot(HomePage);
  }

  ionViewDidLeave(){
    this.general.mostrarSelectorFormulario = true;
  }

  onToFormulario(){
    //console.log(page);
    if (this.page!=""){
      this.navCtrl.setRoot(this.page);
    }

  }

 
  
}
