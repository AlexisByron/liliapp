import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FormularioCrianzaPage } from './formulario-crianza';

@NgModule({
  declarations: [
    FormularioCrianzaPage,
  ],
  imports: [
    IonicPageModule.forChild(FormularioCrianzaPage),
  ],
})
export class FormularioCrianzaPageModule {}
