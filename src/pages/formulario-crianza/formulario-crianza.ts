import { MemoryProvider } from './../../providers/memory/memory';
import { GeneralProvider } from './../../providers/general/general';
import { CommunicationsProvider } from './../../providers/communications/communications';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ToastController } from 'ionic-angular';
import {EntregaAlimentoProvider} from '../../providers/entrega-alimento/entrega-alimento';
import {CrianzaProvider} from '../../providers/crianza/crianza'; 
import { AlertController } from 'ionic-angular';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { LoadingController } from 'ionic-angular';
import {DireccionPage} from '../direccion/direccion';
import 'rxjs/add/operator/timeout';
import {UsuarioProvider} from '../../providers/usuario/usuario';
import {HomePage} from "../home/home";


@IonicPage()
@Component({
  selector: 'page-formulario-crianza',
  templateUrl: 'formulario-crianza.html',
})
export class FormularioCrianzaPage {

  constructor(public loadingCtrl: LoadingController, public navCtrl: NavController, public navParams: NavParams, public coms: CommunicationsProvider, public general: GeneralProvider, private toastCtrl: ToastController, public memory: MemoryProvider,public alimento :EntregaAlimentoProvider,public crianza:CrianzaProvider,public alertCtrl: AlertController,public usuario:UsuarioProvider) {
  }


  public id_sector;
  public pabellones;
  public origenes;
  public permiso;
  //informacion
  
  
    public idCrianza= null;
    public sector= null;
    public idPabellon= null;
   
    public Usuario= null;
    public animal= null;
    public fechaInicio= null;
 
    public origen= null;
    public cantidadInicial= null;
    public sexo= null;
    public pesoPromedioInicial= null;
    public edadInicial= null;
    //campos de ave inicio

    public edadReproductora= null;
    public edadReproductoraNueva= null;
    public edadReproductoraMediana= null;
    public edadReproductoraVieja= null;
    //fin campos ave

    public medidorLuz: null;
    public medidorAgua: null;

  


  public fecha = {
    fechaInicio: null,
    fechaInicioCrianza: null,
    horaInicio: null
  }

  

  ionViewDidLoad(){
    
    this.usuario.verificarSesionPrevia().then(() => {
      if(this.usuario.userSession["nombre"]==""){
          // console.log('no hay datos');
          this.ToHome();
      }else{
          this.general._menu.enable(true,'main-menu');
          this.general.controller = this;
          this.id_sector=(this.usuario.userSession.sector["id"]);
          this.sector=(this.usuario.userSession.sector["nombre_corto"]);
          this.permiso=this.usuario.userSession.sector["animal"];
          // this.obtenerHora();
          this.presentLoadingDefault();
          
      }

  });
    
  }

  ToHome(){
    this.navCtrl.setRoot(HomePage);
  }

 
  presentLoadingDefault() {
    let loading = this.loadingCtrl.create({
      // spinner: 'hide',
      content: 'Cargando información'
    });
  
    loading.present();

    var id_sector={"id_sector":this.id_sector};
    this.alimento.GetPabellones(id_sector).timeout(30000).subscribe((data) => { // Success
      
      
      if(Object.keys(data).length>0){
                  // console.log(data);
                 this.pabellones=(data);
                 this.crianza.GetCrianza(id_sector).timeout(30000).subscribe((data) => { // Success
                  // console.log(data[0]);
                  this.idCrianza=data[0]["nro_crianza"];
                  loading.dismiss();
                },()=>{
                  // console.log("problema al traer crianza");
                });
                  
                
      }else{
        this.showAlert("Error","No se encontraron pabellones");
        this.navCtrl.setRoot(DireccionPage);
      }

     
    }, () => {
      this.showAlert("Error", "No se logro traer informacíon , intentar nuevamente");

    });

    // this.obtenerHora();
  }

  // obtenerHora() {
  //   let fecha = new Date(Date.now());
  //   let hora = new Date(Date.now());
  //   let month = (fecha.getMonth() + 1) + '';
  //   let minutos = (hora.getMinutes() + '');
  //
  //   month = month.length == 1 ? '0' + month : month;
  //
  //   let day = fecha.getDate() + '';
  //   day = day.length == 1 ? '0' + day : day;
  //   minutos = minutos.length == 1 ? '0' + minutos : minutos;
  //
  //   this.fecha.fechaInicio = fecha.getFullYear() + '-' + month + '-' + day;
  //
  //   if(hora.getHours().toString().length==1){
  //     this.fecha.horaInicio = "0"+hora.getHours() + ':' + minutos;
  //   }else{
  //     this.fecha.horaInicio = hora.getHours() + ':' + minutos;
  //   }
  //
  // }

  submit() { 
    var obj={
      "idPabellon": this.idPabellon,
      "peso":this.pesoPromedioInicial,
      "edadReproductora":this.edadReproductora,
      "cantidadInicial": this.cantidadInicial,
      "sexo": this.sexo,
      "edadInicial": this.edadInicial, 
      "medidorLuz": this.medidorLuz,
      "medidorAgua": this.medidorAgua,
      "fecha_registro":this.fecha.fechaInicio+" "+this.fecha.horaInicio,
     //campos de ave inicio      
      "edadReproductoraNueva": this.edadReproductoraNueva,
      "edadReproductoraMediana": this.edadReproductoraMediana,
      "edadReproductoraVieja": this.edadReproductoraVieja,
     //fin campos ave
    }

     //console.log(obj);

    this.crianza.InsertCrianzaPab(obj).timeout(30000).subscribe((data) => { // Success
      this.showAlert("exito","se agrego la crianza al pabellon");
      this.limpar();
    // this.obtenerHora();
    },()=>{
      this.showAlert("Error", "No se logro agregar crianza , intentar nuevamente");
    });

   
    
  }

  limpar(){

    this.idPabellon= null;
 
    this.cantidadInicial= null;
    this.sexo= null;
    this.pesoPromedioInicial= null;
    this.edadInicial= null;
    //campos de ave inicio

    this.edadReproductora= null;
    this.edadReproductoraNueva= null;
    this.edadReproductoraMediana= null;
    this.edadReproductoraVieja= null;
    //fin campos ave

    this.medidorLuz= null;
    this.medidorAgua= null;
    this.fecha.fechaInicio=null;
    this.fecha.horaInicio=null;
  }
  presentToast() {
    let toast = this.toastCtrl.create({
      message: 'Su formulario se ha enviado con éxito',
      duration: 3000,
      position: 'top'
    });
  
    toast.onDidDismiss(() => {
      // console.log('Dismissed toast');
    });
  
    toast.present();
  }

  presentError(message) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: 3000,
      position: 'top'
    });
  
    toast.onDidDismiss(() => {
      // console.log('Dismissed toast');
    });
  
    toast.present();
  }

  showAlert(titulo,mensaje) {
    const alert = this.alertCtrl.create({
      title: titulo,
      subTitle: mensaje,
      buttons: ['OK']
    });
    alert.present();
  }

}
