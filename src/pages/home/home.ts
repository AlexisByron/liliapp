import {GeneralProvider} from '../../providers/general/general';
import {CommunicationsProvider} from '../../providers/communications/communications';
import {Component} from '@angular/core';
import {NavController, IonicPage} from 'ionic-angular';
import {MemoryProvider} from '../../providers/memory/memory';
import {ToastController} from 'ionic-angular/components/toast/toast-controller';
import { ScreenOrientation } from '@ionic-native/screen-orientation';
import { StatusBar } from '@ionic-native/status-bar';
import {UsuarioProvider} from '../../providers/usuario/usuario';


@Component({
    selector: 'page-home',
    templateUrl: 'home.html'
})

export class HomePage {

    public sessionData = {
        username: '',
        password: ''
    };

    constructor(private statusBar: StatusBar, public navCtrl: NavController, private coms: CommunicationsProvider, public memory: MemoryProvider, public general: GeneralProvider, private toastCtrl: ToastController ,public pantalla:ScreenOrientation,public usuario:UsuarioProvider) {

    }

    login() {
        this.general.presentLoader('Iniciando sesión');
        // console.log(this.sessionData);
        this.usuario.login(this.sessionData).timeout(30000).subscribe((usuario) => { // Success
            if(Object.keys(usuario).length>0){
                // console.log(data[0]["username"]);//traer sector

                this.usuario.GetSector({"username":usuario[0]["username"]}).timeout(30000).subscribe((sector) => { // Success
                    if(Object.keys(sector).length>0){
                        // console.log(usuario);
                        // console.log(sector);
                        var user=[{
                            apellido: usuario[0]["apellido"],
                            email: usuario[0]["email"],
                            nombre: usuario[0]["nombre"],
                            password: usuario[0]["password"],
                            rut: usuario[0]["rut"],
                            username: usuario[0]["username"],
                            sector:sector[0]

                        }];
                        // console.log(user[0]);
                        this.usuario.user=user[0];
                        this.general.dismissLoader();
                        this.navCtrl.setRoot('DireccionPage').then();
                    }else{
                        alert("No se encontro sector asociado");
                        this.general.dismissLoader();
                    }

                },()=>{
                    alert("error de servidor, time out");
                    this.general.dismissLoader();

                });

            }else{
                alert("credenciales incorrectas");
                this.general.dismissLoader();
            }

        },()=>{
            alert("error de servidor, time out");
            this.general.dismissLoader();
        });

    }

    ionViewDidLoad() {
        // console.log('sdfsdfsdfdsfdsdsf')
        this.general._menu.enable(false,'main-menu');
        this.usuario.verificarSesionPrevia().then(() => {
            if(this.usuario.userSession["nombre"]!=""){
                //alert("hay datos");
                // console.log(this.usuario.userSession);
                this.navCtrl.setRoot('DireccionPage').then();
            }

        });

    }

    goToMain(page) {
        // solicitar informacion de sector y crianza antes de seguir;
        // luego solicitar control actual en memoria y contrastar con servidor
        // console.log('solicitando info de sector');
        this.general.presentLoader('Sincronizando informacion de sector ' + this.memory.userProfile['sector']);
        this.coms.sectorInfo(this.memory.userProfile['sector'], this.memory.userToken).then(data => {
            this.general.dismissLoader();
            // console.log('se recibio info de sector', data);
            this.navCtrl.setRoot(page).then();
            this.memory.sector = data['sector'];
        }).catch(err => {
            this.general.dismissLoader();
            // console.log('error con info de sector: ', err);
        })
    }

    presentError(message) {
        let toast = this.toastCtrl.create({
            message: message,
            duration: 3000,
            position: 'top'
        });
        toast.onDidDismiss(() => {
            // console.log('Dismissed toast');
        });
        toast.present().then();
    }

}
