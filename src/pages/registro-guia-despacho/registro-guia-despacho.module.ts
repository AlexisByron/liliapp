import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RegistroGuiaDespachoPage } from './registro-guia-despacho';

@NgModule({
  declarations: [
    RegistroGuiaDespachoPage,
  ],
  imports: [
    IonicPageModule.forChild(RegistroGuiaDespachoPage),
  ],
})
export class RegistroGuiaDespachoPageModule {}
