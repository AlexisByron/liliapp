import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { MemoryProvider } from './../../providers/memory/memory';
import { GeneralProvider } from './../../providers/general/general';
import {EntregaAlimentoProvider} from '../../providers/entrega-alimento/entrega-alimento';
import { AlertController } from 'ionic-angular';
import {MortalidadProvider} from '../../providers/mortalidad/mortalidad';
import { LoadingController } from 'ionic-angular';
import {DireccionPage} from '../direccion/direccion';
import {UsuarioProvider} from "../../providers/usuario/usuario";
import 'rxjs/add/operator/timeout';
import {HomePage} from "../home/home";
/**
 * Generated class for the RegistroGuiaDespachoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-registro-guia-despacho',
  templateUrl: 'registro-guia-despacho.html',
})
export class RegistroGuiaDespachoPage {

  constructor(public loadingCtrl: LoadingController, public memory: MemoryProvider, public navCtrl: NavController, public navParams: NavParams, public general: GeneralProvider, public alimento: EntregaAlimentoProvider, public alertCtrl: AlertController, public mortalidad: MortalidadProvider, public usuario: UsuarioProvider) {
  }

  public id_sector;
  public NumeroGuia;
  public SectorGuia;

  public fecha = {
    fechaInicio: null,
    fechaInicioCrianza: null,
    horaInicio: null
  };

  public ListaDietaGuia: any = [];
  public DietaGuia;
  public KilosGuia;

  // obtenerHora() {
  //   let fecha = new Date(Date.now());
  //   let hora = new Date(Date.now());
  //   let month = (fecha.getMonth() + 1) + '';
  //   let minutos = (hora.getMinutes() + '');
  //
  //   month = month.length == 1 ? '0' + month : month;
  //
  //   let day = fecha.getDate() + '';
  //   day = day.length == 1 ? '0' + day : day;
  //   minutos = minutos.length == 1 ? '0' + minutos : minutos;
  //
  //   this.fecha.fechaInicio = fecha.getFullYear() + '-' + month + '-' + day;
  //
  //   if(hora.getHours().toString().length==1){
  //     this.fecha.horaInicio = "0"+hora.getHours() + ':' + minutos;
  //   }else{
  //     this.fecha.horaInicio = hora.getHours() + ':' + minutos;
  //   }
  //
  // }


  ionViewDidLoad() {
    this.usuario.verificarSesionPrevia().then(() => {
      if (this.usuario.userSession["nombre"] == "") {
        // console.log('no hay datos');
        this.ToHome();
      } else {
        this.general._menu.enable(true, 'main-menu');
        this.general.controller = this;
        // this.obtenerHora();
        this.presentLoadingDefault();
      }

    });

    // this.obtenerHora();

  }

  ToHome(){
    this.navCtrl.setRoot(HomePage);
  }

  presentLoadingDefault() {
    let loading = this.loadingCtrl.create({
      // spinner: 'hide',
      content: 'Cargando información'
    });

    loading.present();
    // console.log(this.usuario.userSession);
    // console.log(this.usuario.userSession);
    this.SectorGuia = (this.usuario.userSession.sector["nombre_corto"]);
    loading.dismiss();

    var id_sector = {"id_sector": this.usuario.userSession.sector["id"]};
    this.alimento.GetDietas(id_sector).subscribe((data) => { // Success
      // console.log(data);
      if (Object.keys(data).length > 0) {

        this.ListaDietaGuia = (data);
        // console.log(this.ListaDietaGuia);

      } else {
        this.showAlert("Error", "No existen dietas");
        this.navCtrl.setRoot(DireccionPage);
      }
    });

  }

  Agregar() {
    var guia = {"nro_guia": this.NumeroGuia};


    this.alimento.GetGuias(guia).timeout(30000).subscribe((data) => { // Success


      if (Object.keys(data).length == 0) {
        var objeto = {
          "sector": this.usuario.userSession.sector["id"],
          "nro_guia": this.NumeroGuia,
          "fecha": this.fecha.fechaInicio + " " + this.fecha.horaInicio,
          "dieta": this.DietaGuia,
          "kilosGuia": this.KilosGuia
        };
        // console.log(objeto);
        this.alimento.AgregarGuiaDespacho(objeto).timeout(30000).subscribe((data) => { // Success
          this.showAlert("Exito", "Guía de despacho agregada exitosamente");
          this.NumeroGuia = null;
          this.KilosGuia = null;
          this.DietaGuia = null;
          this.fecha.horaInicio=null;
          this.fecha.fechaInicio=null;

        }, () => {
          this.showAlert("Error", "No se agrego la guía de despacho, intentar nuevamente");

        });
      } else {
        this.showAlert("Error", "Guía de despacho ya está agregada ");
      }
    });


  }



  showAlert(titulo, mensaje) {
    const alert = this.alertCtrl.create({
      title: titulo,
      subTitle: mensaje,
      buttons: ['OK']
    });
    alert.present();
  }

}
