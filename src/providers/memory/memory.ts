import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';

@Injectable()
export class MemoryProvider {

  private _userData = null;

    private _ibeaconListen = true;

    get ibeaconListen(){
        return this._ibeaconListen;
    }

    set ibeaconListen(newVal){
        this._ibeaconListen = newVal;
    }

  /* private _userData = {
    apellido:"",
    email:"",
    empresa:"",
    fono:"",
    nombre:"",
    permisos:"",
    rut:"",
    sector:"",
    username:""
  }; */
  private _token = null;

  private _sector = null;

  private _controlActual: any =  {
    necropsia: 0,
    crianza: '',
    sector: '',
    operario: '',
    sexo: '',
    fecha: new Date(),
    edad: null,
    mortalidad: [],
    comment: ''
};

  public _pabellonesActuales = [];

  constructor(public http: HttpClient, private storage: Storage) {
    // console.log('Hello MemoryProvider Provider');
  }

  get userProfile(){
    return this._userData;
  }

  set user(newVal){
    console.log('nuevo user data');

    this._userData = newVal;
    this.storage.set('TAUD',this._userData).then(()=>{
    console.log('almacenado en storage user data');
    })
  }

  get userToken(){
    console.log(this._token);
    return this._token;
  }

  set token(newVal){
    console.log(' nuvo token');

    this._token = newVal;
    this.storage.set('TAUT',this._token).then(()=>{
    console.log('token almacenado en storage');

    })
  }

  get sector(){
    return this._sector;
  }

  set sector(newVal){
    this._sector = newVal;
  }

  get control(){
    return this._controlActual
  }

  set control(Newval){
    this._controlActual = Newval;
  }

  get informesDiarios () {
    return this._informesDiarios;
  }

  get userInfo(){
    return this._userInfo;
  }

  get crianzaInfo(){
    return this._crianza;
  }

  get sectornfo(){
    return this._sectorInfo;
  }

  verificarSesionPrevia() {
    return new Promise ((resolve,reject) => {

      this.storage.get('TAUT').then(token => {


        if(token == null || token == '' || token == {}) {

          reject('no token')
          console.log('token vacio');
        }
        console.log('token recuperado de mem',token);

        this._token = token

        this.storage.get('TAUD').then(data => {
          console.log("aqui data que fue", data);

         /*  if(data['username'] === "crianza") {
            resolve(true);
          } else if (data['username'] === "contador") {
            resolve(true);
          } */ 
        
            if(!(data == null || data == '' || data == {})) 
            this._userData = data;
            resolve({token : token})
          }
        )   
      }).catch(err => {
        console.log('error al obtener sesion previa');

        reject(err)
      })
    })


  }

  private _d = new Date(Date.now());
  private _dd = new Date(Date.now());
  public informeActual = null; //guardar en localstorage y rescatar de local storage para cada usuario

  private _informesDiarios = [
    {
      id: '123',
      revision: 1,
      necropsia: 1,
      crianza: 189,
      sector: 'La Arboleda',
      sexo: 0,
      fecha: new Date(this._d.setDate(this._d.getDate()-7)),
      edad: [1,1],
      mort: [
        {
          pabellon: 1,
          causales: [
            2,3,0,0,1,0,0,0
          ]
        },
        {
          pabellon: 2,
          causales: [
            2,2,0,0,1,0,1,0
          ]
        }
      ],
      comment: ''
    }
  ]

  private _userInfo = {
    nombre: 'Pedro',
    apellido: 'Perez',
    username: 'pedro.perez',
    password: '12345678',
    email: 'pedro.perez@agrosuper.cl',
    fono: '+56999887744',
    empresa: 'Agrosuper',
    sector: 'La Arboleda',
    credenciales: 'INFTEM',
  }

  private _crianza= {
    no : 189,
    fecha: new Date(this._dd.setDate(this._dd.getDate()-8)),
    sector: 'La Arboleda',
    pabellones : [ 15000,15000,14985,14990,14991,14998,14890 ],
    finalizado: false

  }

  private _sectorInfo ={
    sector: 'La Arboleda',
    crianza: 189,
    pabellones : [ 15000,15000,15000,15000,15000,15000,15000 ],

  }

  

}
