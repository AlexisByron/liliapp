import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Injectable} from '@angular/core';
import 'rxjs/add/operator/timeout';
import {resolveDefinition} from '../../../node_modules/@angular/core/src/view/util';

@Injectable()
export class CommunicationsProvider {

    public beacon_alert = false;

    constructor(public http: HttpClient) {}

    login(username, password) {
        return new Promise((resolve, reject) => {
            console.log('logeando en server');
            let params = {
                "username": username,
                "password": password
            };
            //Establecemos cabeceras
            let headers = new HttpHeaders(); //.set('x-access-token','untoken');

            this.http.post('http://52.191.250.248:2509/api/login', params, {headers: headers}).timeout(10000).subscribe(
                res => {
                    //console.log('se recibio respuesta de login');
                    console.log("aqui el res", res);
                    if (res['success']) {
                       // console.log("si");
                        resolve({userData: res['user'], taut: res['token']});
                    } else {
                        //console.log("no");
                        reject(403);
                    }
                }, err => {
                    console.log('error en login');
                    console.log(err);
                    reject(err);
                }
            );
        })
    }

    perfilUsuario(username, token) {
        return new Promise((resolve, reject) => {
            let params = {
                username: username
            };

            //Establecemos cabeceras
            let headers = new HttpHeaders().set('x-access-token', token);
            this.http.post('http://52.191.250.248:2509/api/user/byName', params, {headers: headers}).timeout(10000).subscribe(
                res => {
                    if (res['success']) {
                        console.log(res);
                        resolve({userData: res['user']});
                    } else {
                        reject('No existe usuario ');
                    }
                }, err => {
                    console.log('error en userprofile');
                    console.log(err);
                    reject(err);
                }
            );
        })

    }

    testToken(token) {
        return new Promise((resolve, reject) => {
            let params = {};
            //Establecemos cabeceras
            let headers = new HttpHeaders().set('x-access-token', token);
            this.http.get('http://52.191.250.248:2509/api/testauth', {headers: headers}).timeout(5000).subscribe(
                res => {
                    //console.log('se recibio respuesta de login');
                    console.log("res en test token", res);
                    resolve(true);
                }, err => {
                    console.log('error en test token');
                    console.log(err);
                    reject(err);
                }
            );
        })
    }

    sectorInfo(sector, token) {
        return new Promise((resolve, reject) => {
            console.log('solicitando info de sector');
            let params = {
                nombre: sector
            };
            //Establecemos cabeceras
            let headers = new HttpHeaders().set('x-access-token', token);
            this.http.post('http://52.191.250.248:2509/api/sector/nombre', params, {headers: headers}).timeout(5000).subscribe(
                res => {
                    if (res['success']) {
                        resolve({sector: res['sector']});
                    } else {
                        reject('No existe sector ');
                    }
                }, err => {
                    console.log('error en sector info');
                    console.log(err);
                    reject(err);
                }
            );
        })
    }


    controlActual(token) {
        return new Promise((resolve, reject) => {
            console.log('solicitando info de sector');
            let params = {};
            //Establecemos cabeceras
            let headers = new HttpHeaders().set('x-access-token', token);

            this.http.get('http://52.191.250.248:2509/api/necropsia/actual', {headers: headers}).timeout(5000).subscribe(
                res => {
                    console.log('se recibio respuesta de necropsiaActual');
                    console.log(res);
                    if (res['success']) {

                        resolve({
                            necropsiaActual: res['user'],
                            causales: res['causales'],
                            sector: res['sector'],
                            capacidad: res['sector']['capacidadPabellones']
                        });
                    } else {
                        reject('No existe necropsiaActual ');
                    }


                }, err => {
                    console.log('error en necropsiaActual');
                    console.log(err);
                    reject(err);
                }
            );
        })

    }

    nuevoControl(control, token) {
        return new Promise((resolve, reject) => {

            console.log('registrando necropsia/registro');


            let params = control;

            console.log(params);


            //Establecemos cabeceras
            let headers = new HttpHeaders().set('x-access-token', token);

            this.http.post('http://52.191.250.248:2509/api/necropsia/registro', params, {headers: headers}).timeout(10000).subscribe(
                res => {
                    console.log('se recibio respuesta de necropsia/registro');
                    console.log(res);
                    resolve(true);


                }, err => {
                    console.log('error en necropsia/registro');
                    console.log(err);
                    reject(err);
                }
            );
        })

    }

    actualizarControl(control, token) {

        return new Promise((resolve, reject) => {

            console.log('registrando necropsia/actualizar');


            let params = control;

            console.log(params);


            //Establecemos cabeceras
            let headers = new HttpHeaders().set('x-access-token', token);

            this.http.post('http://52.191.250.248:2509/api/necropsia/act', params, {headers: headers}).timeout(10000).subscribe(
                res => {
                    console.log('se recibio respuesta de necropsia/actualizar');
                    console.log(res);
                    resolve(true);


                }, err => {
                    console.log('error en necropsia/actualizar');
                    console.log(err);
                    reject(err);
                }
            );
        })

    }


    logout(token) {

        return new Promise((resolve, reject) => {

            console.log('logout');


            let params = {};

            //Establecemos cabeceras
            let headers = new HttpHeaders().set('x-access-token', token);

            this.http.get('http://52.191.250.248:2509/api/logout', {headers: headers}).timeout(5000).subscribe(
                res => {
                    console.log('se recibio respuesta de logout');
                    console.log(res);
                    if (res['success']) {
                        console.log('exito?');
                        resolve(true);
                    } else {
                        reject('No existe logout ');
                    }


                }, err => {
                    console.log('error en logout');
                    console.log(err);
                    reject(err);
                }
            );
        })

    }

    getObject = (token) => {
        let headers = new HttpHeaders().set('x-access-token', token);
        return new Promise((resolve, reject) => {
            this.http.get('http://52.191.250.248:2509/api/contador/get', {headers: headers}).timeout(30000).subscribe(
                res => {
                    let response = res;
                    resolve(response);
                }, err => {
                    console.log(err);
                    reject(err);
                }
            );
        })
    };

    postCrianza = (informacion, token) => {
        let headers = new HttpHeaders().set('x-access-token', token);

        return new Promise((resolve, reject) => {
            let params = informacion;

            this.http.post('http://52.191.250.248:2509/api/crianzaForm/post', params, {headers: headers}).timeout(30000).subscribe(
                res => {
                    console.log(res);
                    resolve(true);
                }, err => {
                    console.log(err);

                    reject(err);
                }
            )
        })
    };

    postCrianzaCerdos = (informacion, token) => {
        let headers = new HttpHeaders().set('x-access-token', token);

        return new Promise((resolve, reject) => {
            let params = informacion;

            this.http.post('http://52.191.250.248:2509/api/crianzaCerdosForm/post', params, {headers: headers}).timeout(30000).subscribe(
                res => {
                    console.log(res);
                    resolve(true);
                }, err => {
                    console.log(err);

                    reject(err);
                }
            )
        })
    };

    postContador = (informacion, token) => {
        let headers = new HttpHeaders().set('x-access-token', token);
        return new Promise((resolve, reject) => {
            let params = informacion;

            this.http.post('http://52.191.250.248:2509/api/contador/post', params, {headers: headers}).timeout(30000).subscribe(
                res => {
                    resolve(true);
                    console.log(res);
                }, err => {
                    console.log(err);
                    reject(err);
                }
            )
        })
    };

    getInfo = (token) => {
        let headers = new HttpHeaders().set('x-access-token', token);
        return new Promise((resolve, reject) => {
            this.http.get('http://52.191.250.248:2509/api/crianzaForm/get', {headers: headers}).timeout(30000).subscribe(
                res => {
                    let response = res;
                    resolve(response);
                }, err => {
                    console.log(err);
                    reject(err);
                }
            );
        })
    };

    getDietaInfo = (token) => {
        let headers = new HttpHeaders().set('x-access-token', token);
        return new Promise((resolve, reject) => {
            this.http.get('http://52.191.250.248:2509/api/dietaForm/get', {headers: headers}).timeout(30000).subscribe(
                res => {
                    let response = res;
                    console.log(response);
                    resolve(response);
                }, err => {
                    console.log(err);
                    reject(err);
                }
            );
        })
    };

    postDieta = (informacion, token) => {
        let headers = new HttpHeaders().set('x-access-token', token);
        return new Promise((resolve, reject) => {
            let params = informacion;
            this.http.post('http://52.191.250.248:2509/api/dietaForm/post', params, {headers: headers}).timeout(30000).subscribe(
                res => {
                    let response = res;
                    resolve(response);
                }, err => {
                    console.log(err);
                    reject(err);
                }
            );
        })
    }


}
