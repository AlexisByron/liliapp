import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
/*
  Generated class for the UsuarioProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class UsuarioProvider {

  private usuario={
    apellido: "",
    email: "",
    nombre: "",
    rut: "",
    username: "",
    sector:[]
  }

  constructor(public http: HttpClient, private storage: Storage) {
    // console.log('Hello UsuarioProvider Provider');
  }

  get userSession(){
    return this.usuario;
  }

  set user(newVal){
    // console.log('nuevo user data');
    this.usuario = newVal;
    this.storage.set('usuario',this.usuario).then(()=>{
      // console.log('almacenado en storage user data');
    })
  }

  verificarSesionPrevia() {
    return new Promise ((resolve,reject) => {

      this.storage.get('usuario').then(usuario => {
        // console.log("aqui data que fue", usuario);

        if(!(usuario == null || usuario == '' || usuario == {}))
          this.usuario = usuario;
        resolve({usuario : usuario})


      }).catch(err => {
        // console.log('error al obtener sesion previa');

        reject(err)
      })
    })


  }

  TestCon(){
    return this.http.get("http://52.234.128.253:1880/TestConApp");
  }

  login(datos){
    return this.http.post("http://52.234.128.253:1880/loginApp",datos);
  }
  GetSector(datos){
    return this.http.post("http://52.234.128.253:1880/getSectorApp",datos);
  }

}
