import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

/*
  Generated class for the CrianzaProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class CrianzaProvider {

  constructor(public http: HttpClient) {
    // console.log('Hello CrianzaProvider Provider');
  }


  InsertCrianzaPab(datos){
    return this.http.post("http://52.234.128.253:1880/InsertCrianzaPabApp",datos);
  }

  GetCrianza(id){
    return this.http.post("http://52.234.128.253:1880/getCrianzaApp",id);
  }

}
