import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

/*
  Generated class for the MortalidadProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class MortalidadProvider {

    constructor(public http: HttpClient) {
        // console.log('Hello MortalidadProvider Provider');
    }

    getMortalidades(id){
        return this.http.post("http://52.234.128.253:1880/getmortalidadesApp",id);
    }

    getMortalidadesAsistidas(id){
        return this.http.post("http://52.234.128.253:1880/getmortalidadesasistidasApp",id);
    }



    InsertMortalidad(data){
        return this.http.post("http://52.234.128.253:1880/insertMortalidadApp",data);
    }

    
}
