import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

/*
  Generated class for the EntregaAlimentoProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class EntregaAlimentoProvider {

  constructor(public http: HttpClient) {
    // console.log('Hello EntregaAlimentoProvider Provider');
  }

  GetDietas(id){
    return this.http.post("http://52.234.128.253:1880/GetDietasApp",id);
  }

  GetSectores(id){
    return this.http.post("http://52.234.128.253:1880/getsectoresApp",id);
  }

  
  GetPabellones(id){
    return this.http.post("http://52.234.128.253:1880/getpabellonApp",id);
  }

  GetIdSilo(id_pabellon){
    return this.http.post("http://52.234.128.253:1880/getidsiloApp",id_pabellon);
  }

  GetGuias(id_guia){
    return this.http.post("http://52.234.128.253:1880/getguiaApp",id_guia);
  }


  AgregarDescargaAlimento(datos){
    return this.http.post("http://52.234.128.253:1880/agregaringresoalimentoApp",datos);
  }

  AgregarGuiaDespacho(datos){
    return this.http.post("http://52.234.128.253:1880/insertguiaApp",datos);
  }





}
