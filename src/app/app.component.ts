import { MenuController } from 'ionic-angular/components/app/menu-controller';
import { GeneralProvider } from '../providers/general/general';
import { MemoryProvider } from '../providers/memory/memory';
import { Component } from '@angular/core';
import {NavController, Platform} from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { HomePage } from '../pages/home/home';
import { CommunicationsProvider } from '../providers/communications/communications';
import { Storage } from '@ionic/storage';
import {UsuarioProvider} from '../providers/usuario/usuario';
@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage:any = HomePage;
  currentPage = null;


  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen, public memory : MemoryProvider, private coms : CommunicationsProvider, public general : GeneralProvider, private storage: Storage, public menuCtrl: MenuController,public user:UsuarioProvider) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      // statusBar.styleDefault();
      splashScreen.hide();

      // let status bar overlay webview
     // statusBar.overlaysWebView(true);

      // set status bar to white
      statusBar.backgroundColorByHexString('#1D568F');
    });
  }


 

  logout(){
    this.general.presentPrompt('Cerrar sesión','¿Esta seguro que desea cerrar sesión?','Si','No').then(()=>{


      this.user.user = {
        apellido: "",
        email: "",
        nombre: "",
        rut: "",
        username: "",
        sector:[]
      };

      this.storage.clear();

      this.general.controller.navCtrl.setRoot(HomePage);


    })
  }

  onToFormularios = ()=> {
    let currentPage = this.general.controller.navCtrl.getActive().name;

    if(currentPage !== "DireccionPage"){
      this.general.controller.navCtrl.setRoot('DireccionPage');
      this.menuCtrl.close();
    }
  }

  OnCheck = () => {

    if(this.currentPage === "DireccionPage") {
      console.log("direccion");
      this.currentPage = false;
    } else {
      console.log("no direccion");
      this.currentPage = true;
    }
  }

  ionViewDidEnter(){
   this.OnCheck();
  }



}

