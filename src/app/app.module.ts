import {BrowserModule} from '@angular/platform-browser';
import {ErrorHandler, NgModule} from '@angular/core';
import {IonicApp, IonicErrorHandler, IonicModule} from 'ionic-angular';
import {SplashScreen} from '@ionic-native/splash-screen';
import {StatusBar} from '@ionic-native/status-bar';

import {MyApp} from './app.component';
import {HomePage} from '../pages/home/home';
import {CommunicationsProvider } from '../providers/communications/communications';
import {GeneralProvider} from '../providers/general/general';
import { MemoryProvider } from '../providers/memory/memory';

import {AndroidPermissions} from '@ionic-native/android-permissions';
import {HttpClientModule} from "@angular/common/http";
import { ScreenOrientation } from '@ionic-native/screen-orientation';

//import {IBeacon} from '@ionic-native/ibeacon';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { IonicStorageModule } from '@ionic/storage';
import { EntregaAlimentoProvider } from '../providers/entrega-alimento/entrega-alimento';
import { CrianzaProvider } from '../providers/crianza/crianza';
import { MortalidadProvider } from '../providers/mortalidad/mortalidad';
import { LoadingController } from 'ionic-angular';
import { UsuarioProvider } from '../providers/usuario/usuario';


@NgModule({
  declarations: [
    MyApp,
    HomePage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    HttpClientModule,
    IonicStorageModule.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    GeneralProvider,
    AndroidPermissions,
    ScreenOrientation,
   // IBeacon,
    InAppBrowser,
    CommunicationsProvider,
    MemoryProvider,
    EntregaAlimentoProvider,
    CrianzaProvider,
    MortalidadProvider,
    LoadingController,
    UsuarioProvider
  ]
})
export class AppModule {}
