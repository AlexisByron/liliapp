-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 20-03-2019 a las 19:58:38
-- Versión del servidor: 10.1.37-MariaDB
-- Versión de PHP: 7.3.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `liliapp`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `area_productiva`
--

CREATE TABLE `area_productiva` (
  `id` int(11) NOT NULL,
  `id_proceso` int(11) NOT NULL,
  `area_productiva` varchar(50) COLLATE utf8_bin NOT NULL,
  `cod_area_productiva` varchar(50) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `bienestar_equipo`
--

CREATE TABLE `bienestar_equipo` (
  `id` int(11) NOT NULL,
  `id_equipo` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `volt_bateria` float DEFAULT NULL,
  `memoria_sd` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `bienestar_equipo_choretime`
--

CREATE TABLE `bienestar_equipo_choretime` (
  `id_equipo` int(11) NOT NULL,
  `temperatura_interior` float DEFAULT NULL,
  `humedad_interior` float DEFAULT NULL,
  `agua` float DEFAULT NULL,
  `equipo_ventilacion` float DEFAULT NULL,
  `estado_equipos_ventilacion` int(11) DEFAULT NULL,
  `gas` float DEFAULT NULL,
  `tipo_ventilacion` int(11) DEFAULT NULL,
  `campana` float DEFAULT NULL,
  `campana_status` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cargo`
--

CREATE TABLE `cargo` (
  `id` int(11) NOT NULL,
  `cargo` varchar(50) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cobb500_pollos`
--

CREATE TABLE `cobb500_pollos` (
  `id` int(11) NOT NULL,
  `timestamp` int(11) DEFAULT NULL,
  `ganancia_diaria` int(11) DEFAULT NULL,
  `ganancia_diaria_promedio` float DEFAULT NULL,
  `conversion_alimenticia_acumulada` float DEFAULT NULL,
  `consumo_alimento_acumulado` int(11) DEFAULT NULL,
  `consumo_diario_alimento` int(11) DEFAULT NULL,
  `peso_para_edad` int(11) DEFAULT NULL,
  `edad` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cobb500_pollos_hembras`
--

CREATE TABLE `cobb500_pollos_hembras` (
  `id` int(11) DEFAULT NULL,
  `edad` int(11) DEFAULT NULL,
  `peso_para_edad` int(11) DEFAULT NULL,
  `crecimiento_diario` double DEFAULT NULL,
  `ganancia_diaria_promedio` double DEFAULT NULL,
  `conversion_alimenticia_acumulada` double DEFAULT NULL,
  `consumo_diario_alimento` int(11) DEFAULT NULL,
  `consumo_alimento_acumulado` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cobb500_pollos_machos`
--

CREATE TABLE `cobb500_pollos_machos` (
  `id` int(11) DEFAULT NULL,
  `timestamp` int(11) DEFAULT NULL,
  `edad` int(11) DEFAULT NULL,
  `peso_para_edad` int(11) DEFAULT NULL,
  `crecimiento_diario` float DEFAULT NULL,
  `ganancia_diaria_promedio` double DEFAULT NULL,
  `conversion_alimenticia_acumulada` double DEFAULT NULL,
  `consumo_diario_alimento` int(11) DEFAULT NULL,
  `consumo_alimento_acumulado` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `crianza`
--

CREATE TABLE `crianza` (
  `id` int(11) NOT NULL,
  `id_sector` int(11) DEFAULT NULL,
  `fecha` datetime DEFAULT NULL,
  `nro_crianza` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `crianza`
--

INSERT INTO `crianza` (`id`, `id_sector`, `fecha`, `nro_crianza`) VALUES
(1, 1, '2019-03-19 18:45:00', 1),
(2, 2, '2019-03-19 18:45:33', 1),
(3, 3, '2019-03-19 18:45:36', 1),
(4, 3, '2019-03-20 15:24:12', 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `crianza_pabellon`
--

CREATE TABLE `crianza_pabellon` (
  `id` int(11) NOT NULL,
  `id_pabellon` int(11) DEFAULT NULL,
  `fecha_inicio` datetime DEFAULT NULL,
  `cantidad_animales` int(11) DEFAULT NULL,
  `peso_promedio_inicial` double DEFAULT NULL,
  `edad_inicial` int(11) DEFAULT NULL,
  `edad_reproductora` int(11) DEFAULT NULL,
  `cantidad_nuevos` int(11) DEFAULT NULL,
  `cantidad_medianos` int(11) DEFAULT NULL,
  `cantidad_viejos` int(11) DEFAULT NULL,
  `sexo` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `medidor_agua` int(11) DEFAULT NULL,
  `medidor_luz` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `crianza_pabellon`
--

INSERT INTO `crianza_pabellon` (`id`, `id_pabellon`, `fecha_inicio`, `cantidad_animales`, `peso_promedio_inicial`, `edad_inicial`, `edad_reproductora`, `cantidad_nuevos`, `cantidad_medianos`, `cantidad_viejos`, `sexo`, `medidor_agua`, `medidor_luz`) VALUES
(7, 5, '2019-03-19 17:36:00', 10, 9, 2, 2, 2, 1, 0, 'Macho', 5, 5),
(8, 48, '2019-03-19 17:37:00', 10, 19, 2, 9, NULL, NULL, NULL, 'Hembra', 8, 8),
(9, 52, '2019-02-20 12:21:00', 90, 10, 5, 1, NULL, NULL, NULL, 'Macho', 10, 10);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalle_dieta_sectores`
--

CREATE TABLE `detalle_dieta_sectores` (
  `id` int(11) NOT NULL,
  `id_sector` int(11) DEFAULT NULL,
  `id_dieta` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `detalle_dieta_sectores`
--

INSERT INTO `detalle_dieta_sectores` (`id`, `id_sector`, `id_dieta`) VALUES
(1, 1, 1),
(2, 1, 2),
(3, 1, 3),
(4, 1, 4),
(5, 2, 5),
(6, 2, 6),
(7, 2, 7),
(8, 2, 8),
(9, 2, 9),
(14, 3, 5),
(15, 3, 6),
(16, 3, 7),
(17, 3, 8),
(18, 3, 9),
(19, 3, 10),
(20, 3, 11),
(21, 3, 12),
(22, 3, 13);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `dieta`
--

CREATE TABLE `dieta` (
  `id` int(11) NOT NULL,
  `dieta` text COLLATE utf8_bin NOT NULL,
  `dieta_label` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `costo_kg` float DEFAULT NULL COMMENT 'valor $ kg',
  `codigo_dieta` varchar(50) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `dieta`
--

INSERT INTO `dieta` (`id`, `dieta`, `dieta_label`, `costo_kg`, `codigo_dieta`) VALUES
(1, 'Broiler Prestarter', 'Prestarter', 258.33, '01BGPCP'),
(2, 'Broiler Inicial', 'Inicial', 226.29, '01BGICP'),
(3, 'Broiler Mediana', 'Mediana', 213.98, '01BGMCP'),
(4, 'Broiler Final', 'Final', 208.91, '01BGFCP'),
(5, 'Crianza Ambos', 'Crianza Ambos', 192, '1'),
(6, 'Engorda Hembra', 'Engorda Hembra', 187, '1'),
(7, 'Engorda Final Ambos', 'Engorda Final Ambos', 176, '1'),
(8, 'Final Retiro Ambos', 'Final Retiro Ambos', 169, '1'),
(9, 'Retiro Machos', 'Retiro Machos', 163, '1'),
(10, 'Nan', 'Nan', 1, '1'),
(11, 'Inicial', 'Inicial', 1, '1'),
(12, 'Mediano', 'Mediano', 1, '1'),
(13, 'Recrias', 'Recrias', 1, '1');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `dieta_historico`
--

CREATE TABLE `dieta_historico` (
  `id` int(11) NOT NULL,
  `id_dieta` int(11) NOT NULL,
  `id_pabellon` int(11) DEFAULT NULL,
  `fecha` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `kilos` int(11) NOT NULL,
  `id_silo` int(11) DEFAULT NULL,
  `kilos_iot` int(11) DEFAULT NULL,
  `diferencia` int(11) DEFAULT NULL,
  `id_guia` int(11) DEFAULT NULL,
  `fecha_registro` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `dieta_historico`
--

INSERT INTO `dieta_historico` (`id`, `id_dieta`, `id_pabellon`, `fecha`, `kilos`, `id_silo`, `kilos_iot`, `diferencia`, `id_guia`, `fecha_registro`) VALUES
(1, 5, 54, '2019-03-19 14:06:00', 9, 112, NULL, NULL, 1, '2019-03-19 14:07:07'),
(2, 5, 47, '2019-03-19 14:08:00', 9, 97, NULL, NULL, 1, '2019-03-19 14:08:29'),
(3, 5, 48, '2019-01-19 14:08:00', 2, 99, NULL, NULL, 1, '2019-03-19 14:09:07'),
(4, 5, 48, '2019-01-19 14:08:00', 2, 100, NULL, NULL, 1, '2019-03-19 14:09:07'),
(5, 10, 65, '2019-03-20 15:20:00', 5, 134, NULL, NULL, 2, '2019-03-20 15:21:33');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `equipos`
--

CREATE TABLE `equipos` (
  `id` int(11) NOT NULL,
  `id_tipo_equipo` int(11) NOT NULL,
  `id_pabellon` int(11) NOT NULL,
  `numero_serie` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `version_hardware` varchar(20) COLLATE utf8_bin DEFAULT NULL,
  `fabricante` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `modelo` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `version_firmware` varchar(20) COLLATE utf8_bin DEFAULT NULL,
  `descripcion_ubicacion` text COLLATE utf8_bin,
  `status` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `equipo_tipo`
--

CREATE TABLE `equipo_tipo` (
  `id` int(11) NOT NULL,
  `equipo` varchar(50) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Tipos de Equipo, pertenecen a la tabla equpos';

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estandar_cerdos`
--

CREATE TABLE `estandar_cerdos` (
  `id_estandar` int(11) NOT NULL,
  `edad` int(11) DEFAULT NULL,
  `peso_vivo` float DEFAULT NULL,
  `ganancia_diaria` float DEFAULT NULL,
  `consumo_alimento` float DEFAULT NULL,
  `conversion_alimento` float DEFAULT NULL,
  `ganancia_diaria_acumulada` float DEFAULT NULL,
  `consumo_alimento_acumulado` float DEFAULT NULL,
  `conversion_alimento_acumulado` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `gerencias`
--

CREATE TABLE `gerencias` (
  `id` int(11) NOT NULL,
  `gerencia_subgerencia` varchar(50) COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `guias_despacho`
--

CREATE TABLE `guias_despacho` (
  `id` int(20) NOT NULL,
  `dieta_id` int(11) DEFAULT NULL,
  `fecha` datetime DEFAULT NULL,
  `sector_id` int(11) DEFAULT NULL,
  `kilos` int(11) DEFAULT NULL,
  `fecha_registro` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `guias_despacho`
--

INSERT INTO `guias_despacho` (`id`, `dieta_id`, `fecha`, `sector_id`, `kilos`, `fecha_registro`) VALUES
(1, 5, '2019-03-17 12:07:00', 2, 10, '2019-03-18 15:11:02'),
(2, 10, '2019-03-20 12:20:00', 3, 100, '2019-03-20 15:20:27');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `itr`
--

CREATE TABLE `itr` (
  `identificador` int(11) NOT NULL,
  `correlativo` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `Etapa` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `Sector` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `Lugar_trabajo` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `Origen` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `Contratista` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `Tipo_trabajo` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `Descripcion_trabajo` text COLLATE utf8_bin,
  `Identificador_trabajo_anterior` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `Tiene_orden_compra` varchar(20) COLLATE utf8_bin DEFAULT NULL,
  `Orden_compra` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `Tiene_numero_ticket` varchar(20) COLLATE utf8_bin DEFAULT NULL,
  `Ticket` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `otro` text COLLATE utf8_bin,
  `pabellones_involucrados` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `Fecha_solicitud` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `Hora_solicitud` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `Fecha_inicio` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `Hora_inicio` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `Fecha_fin` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `Hora_fin` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `Tipo_maquinaria` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `Numero_horas_maquinaria` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `Numero_personas` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `Detalle_trabajo_realizado` text COLLATE utf8_bin,
  `Generacion_residuos` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `Contratista_retira_residuos` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `Residuos_generados` text COLLATE utf8_bin,
  `Responsable_solicitud` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `Responsable_recepcion_solicitud` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `Empresa_responsable_contratista` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `Nombre_responsable_contratista` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `itr_bk`
--

CREATE TABLE `itr_bk` (
  `identificador` int(11) NOT NULL,
  `correlativo` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `Etapa` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `Sector` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `Lugar_trabajo` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `Origen` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `Contratista` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `Tipo_trabajo` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `Descripcion_trabajo` text COLLATE utf8_bin,
  `Identificador_trabajo_anterior` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `Tiene_orden_compra` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `Orden_compra` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `Tiene_numero_ticket` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `Ticket` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `otro` text COLLATE utf8_bin,
  `pabellones_involucrados` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `Fecha_solicitud` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `Hora_solicitud` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `Fecha_inicio` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `Hora_inicio` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `Fecha_fin` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `Hora_fin` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `Tipo_maquinaria` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `Numero_horas_maquinaria` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `Numero_personas` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `Detalle_trabajo_realizado` text COLLATE utf8_bin,
  `Generacion_residuos` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `Contratista_retira_residuos` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `Residuos_generados` text COLLATE utf8_bin,
  `Responsable_solicitud` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `Responsable_recepcion_solicitud` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `Empresa_responsable_contratista` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `Nombre_responsable_contratista` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `kpi_cerdos`
--

CREATE TABLE `kpi_cerdos` (
  `id` int(11) NOT NULL,
  `id_pabellon` int(11) DEFAULT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `edad` int(11) DEFAULT NULL,
  `consumo_diario_alimento` int(11) DEFAULT NULL,
  `consumo_acumulado_alimento` int(11) DEFAULT NULL,
  `stock_alimento` int(11) DEFAULT NULL,
  `animales_totales` int(11) DEFAULT NULL,
  `edad_animales` int(11) DEFAULT NULL,
  `peso_promedio_animal` float DEFAULT NULL,
  `consumo_per_capita` float DEFAULT NULL,
  `ganancia` float DEFAULT NULL,
  `conversion` float DEFAULT NULL,
  `mortalidad_diaria` int(11) DEFAULT NULL,
  `mortalidad_acumulada` int(11) DEFAULT NULL,
  `porcentaje_mortalidad` int(11) DEFAULT NULL,
  `consumo_agua` int(11) DEFAULT NULL,
  `consumo_alimento_dieta_edad` int(11) DEFAULT NULL,
  `temperatura_exterior_minima` int(11) DEFAULT NULL,
  `temperatura_exterior_promedio` int(11) DEFAULT NULL,
  `temperatura_interior_promedio` int(11) DEFAULT NULL,
  `humedad_exterior_promedio` int(11) DEFAULT NULL,
  `humedad_interior_promedio` int(11) DEFAULT NULL,
  `co2_promedio` int(11) DEFAULT NULL,
  `co_promedio` int(11) DEFAULT NULL,
  `nh3_promedio` int(11) DEFAULT NULL,
  `peso_medio` int(11) DEFAULT NULL,
  `temperatura_exterior_maxima` int(11) DEFAULT NULL,
  `carga_iot` int(11) DEFAULT NULL,
  `crianza` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `kpi_diarios`
--

CREATE TABLE `kpi_diarios` (
  `id` int(11) NOT NULL,
  `id_pabellon` int(11) DEFAULT NULL,
  `timestamp` datetime DEFAULT NULL,
  `edad` int(11) DEFAULT NULL,
  `consumo_diario_alimento` int(11) DEFAULT NULL,
  `consumo_acumulado_alimento` int(11) DEFAULT NULL,
  `consumo_per_capita` int(11) DEFAULT NULL,
  `consumo_acumulado_per_capita` int(11) DEFAULT NULL,
  `stock_alimento` int(11) DEFAULT NULL,
  `animales_totales` int(11) DEFAULT NULL,
  `edad_animales` int(11) DEFAULT NULL,
  `peso_promedio_animal` float DEFAULT NULL,
  `ganancia` float DEFAULT NULL,
  `ganancia_promedio_acumulada` float DEFAULT NULL,
  `conversion` float DEFAULT NULL,
  `mortalidad_diaria` int(11) DEFAULT NULL,
  `mortalidad_acumulada` int(11) DEFAULT NULL,
  `porcentaje_mortalidad` float DEFAULT NULL,
  `consumo_agua` int(11) DEFAULT NULL,
  `consumo_alimento_dieta_edad` int(11) DEFAULT NULL,
  `temperatura_exterior_minima` int(11) DEFAULT NULL,
  `temperatura_exterior_promedio` float DEFAULT NULL,
  `temperatura_interior_promedio` float DEFAULT NULL,
  `humedad_exterior_promedio` float DEFAULT NULL,
  `humedad_interior_promedio` float DEFAULT NULL,
  `co2_promedio` float DEFAULT NULL,
  `co_promedio` float DEFAULT NULL,
  `nh3_promedio` float DEFAULT NULL,
  `peso_medio` int(11) DEFAULT NULL,
  `temperatura_exterior_maxima` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `macrozona`
--

CREATE TABLE `macrozona` (
  `id` int(11) NOT NULL,
  `id_region` int(11) NOT NULL,
  `macrozona` varchar(50) COLLATE utf8_bin NOT NULL,
  `cod_macrozona` varchar(20) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `macrozona`
--

INSERT INTO `macrozona` (`id`, `id_region`, `macrozona`, `cod_macrozona`) VALUES
(1, 7, 'Rancagua', '1');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `medicion_pabellon`
--

CREATE TABLE `medicion_pabellon` (
  `id` int(11) NOT NULL,
  `id_equipo` int(11) NOT NULL,
  `temperatura_interior` float NOT NULL,
  `humedad_interior` float NOT NULL,
  `co2` float NOT NULL,
  `co` float NOT NULL,
  `nh3` float NOT NULL,
  `peso_pollo` float NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `modelos_crecimiento_aves`
--

CREATE TABLE `modelos_crecimiento_aves` (
  `id` int(11) NOT NULL,
  `id_modelo` int(11) DEFAULT NULL,
  `a` float DEFAULT NULL,
  `b` float DEFAULT NULL,
  `c` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mortalidad`
--

CREATE TABLE `mortalidad` (
  `id` int(11) NOT NULL,
  `mortalidad` varchar(50) COLLATE utf8_bin NOT NULL,
  `enfermedad` tinyint(1) NOT NULL,
  `asistido` tinyint(1) DEFAULT '0',
  `animal` varchar(50) COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `mortalidad`
--

INSERT INTO `mortalidad` (`id`, `mortalidad`, `enfermedad`, `asistido`, `animal`) VALUES
(1, 'Muerte súbita', 0, 0, 'aves'),
(2, 'Chicos', 0, 1, 'aves'),
(3, 'Problemas patas', 0, 1, 'aves'),
(4, 'Coccidia', 1, 0, 'aves'),
(5, 'Ascitis', 1, 1, 'aves'),
(6, 'Retención Bitelo', 1, 0, 'aves'),
(7, 'Colibacilosis', 1, 1, 'aves'),
(8, 'Otros', 0, 1, 'aves'),
(9, 'Sin causa', 0, 0, 'aves'),
(10, 'Alergia', 0, 0, 'cerdos'),
(11, 'Asfixia', 0, 0, 'cerdos'),
(12, 'Atresia Anal', 0, 0, 'cerdos'),
(13, 'Bajo Desarrollo', 0, 0, 'cerdos'),
(14, 'Canibalismo', 0, 0, 'cerdos'),
(15, 'Cargado Faenadora', 0, 0, 'cerdos'),
(16, 'Cojera', 0, 0, 'cerdos'),
(17, 'Diarrea Severa', 0, 0, 'cerdos'),
(18, 'Endocarditis Valvular', 0, 0, 'cerdos'),
(19, 'Epidermitis', 0, 0, 'cerdos'),
(20, 'Hemorragia Interna', 0, 0, 'cerdos'),
(21, 'Hernia Inguinal', 0, 0, 'cerdos'),
(22, 'Hernia Umbilical', 0, 0, 'cerdos'),
(23, 'Ileitis', 0, 0, 'cerdos'),
(24, 'Lesion Inguinal', 0, 0, 'cerdos'),
(25, 'Miscelaneos', 0, 0, 'cerdos'),
(26, 'Muestra Laboratorio', 0, 0, 'cerdos'),
(27, 'Neumonía', 0, 0, 'cerdos'),
(28, 'Neumonía Atípica', 0, 0, 'cerdos'),
(29, 'No Viable(1 a 3 días)', 0, 0, 'cerdos'),
(30, 'Pericarditis', 0, 0, 'cerdos'),
(31, 'Peritonitis', 0, 0, 'cerdos'),
(32, 'Poliserositis', 0, 0, 'cerdos'),
(33, 'Robo', 0, 0, 'cerdos'),
(34, 'Ruptura Hepática', 0, 0, 'cerdos'),
(35, 'Asistencia', 0, 0, 'cerdos'),
(36, 'Septicemia', 0, 0, 'cerdos'),
(37, 'Sindrome de Insuficiencia Cardiaca', 0, 0, 'cerdos'),
(38, 'Sindrome de Estrés Porcino', 0, 0, 'cerdos'),
(39, 'Torsión Intestinal', 0, 0, 'cerdos'),
(40, 'Transporte', 0, 0, 'cerdos'),
(41, 'Úlcera Gástrica', 0, 0, 'cerdos'),
(42, 'Úlcera Crónica', 0, 0, 'cerdos'),
(43, 'Exámenes', 0, 1, 'cerdos');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pabellon`
--

CREATE TABLE `pabellon` (
  `id` int(11) NOT NULL,
  `identificador_pabellon` int(11) NOT NULL,
  `nro_pabellon` int(11) DEFAULT NULL,
  `cod_pabellon` varchar(20) COLLATE utf8_bin NOT NULL,
  `id_equipo_chore_time` int(11) DEFAULT NULL,
  `id_equipo_silo` int(11) DEFAULT NULL,
  `id_sector` int(11) NOT NULL,
  `id_equipo_gases` int(11) DEFAULT NULL,
  `poligono` varchar(500) COLLATE utf8_bin DEFAULT NULL,
  `poligono2` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `extra` varchar(255) COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `pabellon`
--

INSERT INTO `pabellon` (`id`, `identificador_pabellon`, `nro_pabellon`, `cod_pabellon`, `id_equipo_chore_time`, `id_equipo_silo`, `id_sector`, `id_equipo_gases`, `poligono`, `poligono2`, `extra`) VALUES
(1, 7, 7, 'AVE3P07', 91, 7, 1, 43, '[{\"lat\": -34.205089008091846, \"lng\": -70.87038242223777}, {\"lat\": -34.20462020283436, \"lng\": -70.86889288420669}, {\"lat\": -34.204784456068026, \"lng\": -70.86881840730513}, {\"lat\": -34.20522930696856, \"lng\": -70.8703162205475}, {\"lat\": -34.205089008091846, \"lng\": -70.87038242223777}]', NULL, NULL),
(2, 2, 6, 'AVE3P06', 90, 6, 1, 44, '[{\"lat\": -34.20481525351371, \"lng\": -70.87050655040703}, {\"lat\": -34.20435329064705, \"lng\": -70.86901701237595}, {\"lat\": -34.20450727855058, \"lng\": -70.86892598505182}, {\"lat\": -34.20495213091392, \"lng\": -70.8704444863224}, {\"lat\": -34.20481525351371, \"lng\": -70.87050655040703}]', NULL, NULL),
(3, 3, 5, 'AVE3P05', 89, 5, 1, 45, '[{\"lat\": -34.20455518584096, \"lng\": -70.87064722899885}, {\"lat\": -34.204072689743974, \"lng\": -70.86912459012262}, {\"lat\": -34.20421983423647, \"lng\": -70.86905838843236}, {\"lat\": -34.20470232949119, \"lng\": -70.87056861449165}, {\"lat\": -34.20455518584096, \"lng\": -70.87064722899885}]', NULL, NULL),
(4, 4, 4, 'AVE3P04', 88, 4, 1, 46, '[{\"lat\": -34.20427800756962, \"lng\": -70.87075480674554}, {\"lat\": -34.20382972967093, \"lng\": -70.86923630547496}, {\"lat\": -34.20397003064361, \"lng\": -70.86917010378468}, {\"lat\": -34.20441488584222, \"lng\": -70.87067619223835}, {\"lat\": -34.20427800756962, \"lng\": -70.87075480674554}]', NULL, NULL),
(5, 5, 3, 'AVE3P03', 87, 3, 1, 47, '[{\"lat\": -34.204028204149175, \"lng\": -70.8708789349148}, {\"lat\": -34.20355597100311, \"lng\": -70.86935215843293}, {\"lat\": -34.20371338234573, \"lng\": -70.8692900943483}, {\"lat\": -34.20416166086316, \"lng\": -70.87079204519631}, {\"lat\": -34.204028204149175, \"lng\": -70.8708789349148}]', NULL, NULL),
(6, 6, 2, 'AVE3P02', 86, 2, 1, 48, '[{\"lat\": -34.20374760216393, \"lng\": -70.87098651266149}, {\"lat\": -34.203289055446014, \"lng\": -70.86947214899655}, {\"lat\": -34.20343277931256, \"lng\": -70.86941008491192}, {\"lat\": -34.203894747223885, \"lng\": -70.87091203575993}, {\"lat\": -34.20374760216393, \"lng\": -70.87098651266149}]', NULL, NULL),
(7, 1, 1, 'AVE3P01', 85, 1, 1, 49, '[{\"lat\": -34.203473843229396, \"lng\": -70.87109409040818}, {\"lat\": -34.203011873011405, \"lng\": -70.86957972674324}, {\"lat\": -34.203162441360554, \"lng\": -70.86950111223604}, {\"lat\": -34.20362783273947, \"lng\": -70.87102375111226}, {\"lat\": -34.203473843229396, \"lng\": -70.87109409040818}]', NULL, NULL),
(8, 14, 14, 'AVE3P14', 98, 14, 1, 50, '[{\"lat\": -34.203980296559266, \"lng\": -70.87272430703108}, {\"lat\": -34.20351148513563, \"lng\": -70.87120994336615}, {\"lat\": -34.203651786637984, \"lng\": -70.87112719125331}, {\"lat\": -34.20413428514418, \"lng\": -70.87267051815775}, {\"lat\": -34.203980296559266, \"lng\": -70.87272430703108}]', NULL, NULL),
(9, 13, 13, 'AVE3P13', 97, 13, 1, 51, '[{\"lat\": -34.20424720992766, \"lng\": -70.87260431646746}, {\"lat\": -34.20378524394792, \"lng\": -70.87109409040819}, {\"lat\": -34.20391870104655, \"lng\": -70.87101133829535}, {\"lat\": -34.20438751020548, \"lng\": -70.87253397717157}, {\"lat\": -34.20424720992766, \"lng\": -70.87260431646746}]', NULL, NULL),
(10, 12, 12, 'AVE3P12', 96, 12, 1, 52, '[{\"lat\": -34.204527810249786, \"lng\": -70.87250087632643}, {\"lat\": -34.20404531399609, \"lng\": -70.87096996223893}, {\"lat\": -34.20418903657323, \"lng\": -70.87089962294301}, {\"lat\": -34.20467153200426, \"lng\": -70.87242226181924}, {\"lat\": -34.204527810249786, \"lng\": -70.87250087632643}]', NULL, NULL),
(11, 11, 11, 'AVE3P11', 95, 11, 1, 53, '[{\"lat\": -34.20479129994582, \"lng\": -70.8723684729459}, {\"lat\": -34.20432591499031, \"lng\": -70.87087065970351}, {\"lat\": -34.204483324895165, \"lng\": -70.87079204519631}, {\"lat\": -34.20493844318388, \"lng\": -70.87230227125562}, {\"lat\": -34.20479129994582, \"lng\": -70.8723684729459}]', NULL, NULL),
(12, 10, 10, 'AVE3P10', 94, 10, 1, 54, '[{\"lat\": -34.20505136689007, \"lng\": -70.87225675759355}, {\"lat\": -34.20459282726431, \"lng\": -70.8707299811117}, {\"lat\": -34.204726283084376, \"lng\": -70.87065550421013}, {\"lat\": -34.205184821984155, \"lng\": -70.87217814308636}, {\"lat\": -34.20505136689007, \"lng\": -70.87225675759355}]', NULL, NULL),
(13, 9, 9, 'AVE3P09', 93, 9, 1, 55, '[{\"lat\": -34.20531485494951, \"lng\": -70.8721533174525}, {\"lat\": -34.20484605094812, \"lng\": -70.87061826575936}, {\"lat\": -34.20499319409063, \"lng\": -70.87053965125216}, {\"lat\": -34.205472263007614, \"lng\": -70.87205401491711}, {\"lat\": -34.20531485494951, \"lng\": -70.8721533174525}]', NULL, NULL),
(14, 8, 8, 'AVE3P08', 92, 8, 1, 56, '[{\"lat\": -34.205588607905106, \"lng\": -70.87202091407197}, {\"lat\": -34.20510611772347, \"lng\": -70.8704941375901}, {\"lat\": -34.205249838491895, \"lng\": -70.87041966068854}, {\"lat\": -34.20572548404951, \"lng\": -70.87196712519861}, {\"lat\": -34.205588607905106, \"lng\": -70.87202091407197}]', NULL, NULL),
(15, 20, 20, 'AVE3P20', 104, 20, 1, 57, '[{\"lat\": -34.20584867238944, \"lng\": -70.87385387337132}, {\"lat\": -34.20536960561175, \"lng\": -70.87232295928382}, {\"lat\": -34.20551332593096, \"lng\": -70.87224848238226}, {\"lat\": -34.205992391891776, \"lng\": -70.87377939646977}, {\"lat\": -34.20584867238944, \"lng\": -70.87385387337132}]', NULL, NULL),
(16, 19, 19, 'AVE3P19', 103, 19, 1, 58, '[{\"lat\": -34.205578342185326, \"lng\": -70.87396558872366}, {\"lat\": -34.20511296157513, \"lng\": -70.87244294984744}, {\"lat\": -34.20525668233191, \"lng\": -70.87236019773461}, {\"lat\": -34.20573574975138, \"lng\": -70.87388697421646}, {\"lat\": -34.205578342185326, \"lng\": -70.87396558872366}]', NULL, NULL),
(17, 18, 18, 'AVE3P18', 102, 18, 1, 59, '[{\"lat\": -34.20531827686695, \"lng\": -70.87407316647034}, {\"lat\": -34.20484605094812, \"lng\": -70.87257121562234}, {\"lat\": -34.20498977216, \"lng\": -70.87251328914334}, {\"lat\": -34.20545173153845, \"lng\": -70.87400696478008}, {\"lat\": -34.20531827686695, \"lng\": -70.87407316647034}]', NULL, NULL),
(18, 17, 17, 'AVE3P17', 101, 17, 1, 60, '[{\"lat\": -34.20505136689007, \"lng\": -70.87420556985087}, {\"lat\": -34.20457229558093, \"lng\": -70.87267879336902}, {\"lat\": -34.20472286114292, \"lng\": -70.87261259167876}, {\"lat\": -34.205201931596314, \"lng\": -70.87412281773804}, {\"lat\": -34.20505136689007, \"lng\": -70.87420556985087}]', NULL, NULL),
(19, 16, 16, 'AVE3P16', 100, 16, 1, 61, '[{\"lat\": -34.20478787800699, \"lng\": -70.87431314759758}, {\"lat\": -34.20429853932468, \"lng\": -70.87279878393264}, {\"lat\": -34.20444568342291, \"lng\": -70.87273258224238}, {\"lat\": -34.204917911584694, \"lng\": -70.87425108351295}, {\"lat\": -34.20478787800699, \"lng\": -70.87431314759758}]', NULL, NULL),
(20, 15, 15, 'AVE3P15', 99, 15, 1, 62, '[{\"lat\": -34.204517544400794, \"lng\": -70.87444141337248}, {\"lat\": -34.204021360209424, \"lng\": -70.87291049928497}, {\"lat\": -34.204182192646535, \"lng\": -70.8728442975947}, {\"lat\": -34.20465784422868, \"lng\": -70.8743752116822}, {\"lat\": -34.204517544400794, \"lng\": -70.87444141337248}]', NULL, NULL),
(21, 26, 26, 'AVE3P26', 110, 26, 1, 63, '[{\"lat\": -34.20507189845676, \"lng\": -70.87622058379849}, {\"lat\": -34.204589405317435, \"lng\": -70.87468553210536}, {\"lat\": -34.20474681473025, \"lng\": -70.8746110552038}, {\"lat\": -34.20520877544021, \"lng\": -70.87615438210823}, {\"lat\": -34.20507189845676, \"lng\": -70.87622058379849}]', NULL, NULL),
(22, 25, 25, 'AVE3P25', 109, 25, 1, 64, '[{\"lat\": -34.205335386452035, \"lng\": -70.87610059323488}, {\"lat\": -34.20485631675709, \"lng\": -70.87455312872481}, {\"lat\": -34.20500003795146, \"lng\": -70.87449106464018}, {\"lat\": -34.205482528740326, \"lng\": -70.87600956591076}, {\"lat\": -34.205335386452035, \"lng\": -70.87610059323488}]', NULL, NULL),
(23, 24, 24, 'AVE3P24', 108, 24, 1, 65, '[{\"lat\": -34.20559887362365, \"lng\": -70.87598060267126}, {\"lat\": -34.205140336976264, \"lng\": -70.87444141337248}, {\"lat\": -34.20526010425171, \"lng\": -70.87437107407656}, {\"lat\": -34.205728905950274, \"lng\": -70.87589785055843}, {\"lat\": -34.20559887362365, \"lng\": -70.87598060267126}]', NULL, NULL),
(24, 23, 23, 'AVE3P23', 107, 23, 1, 66, '[{\"lat\": -34.20588289134079, \"lng\": -70.87586474971329}, {\"lat\": -34.20541066858512, \"lng\": -70.87431728520322}, {\"lat\": -34.205554388834294, \"lng\": -70.87425522111859}, {\"lat\": -34.20602661078481, \"lng\": -70.87576544717788}, {\"lat\": -34.20588289134079, \"lng\": -70.87586474971329}]', NULL, NULL),
(25, 22, 22, 'AVE3P22', 106, 22, 1, 67, '[{\"lat\": -34.206132689264855, \"lng\": -70.8757364839384}, {\"lat\": -34.20568442122953, \"lng\": -70.87418488182269}, {\"lat\": -34.205824719115206, \"lng\": -70.87413936816061}, {\"lat\": -34.20626956452575, \"lng\": -70.87566200703684}, {\"lat\": -34.206132689264855, \"lng\": -70.8757364839384}]', NULL, NULL),
(26, 21, 21, 'AVE3P21', 105, 21, 1, 68, '[{\"lat\": -34.20640643956433, \"lng\": -70.87562476858606}, {\"lat\": -34.205941063526225, \"lng\": -70.87408557928727}, {\"lat\": -34.20607793909828, \"lng\": -70.8740276528083}, {\"lat\": -34.20653989251297, \"lng\": -70.87556270450142}, {\"lat\": -34.20640643956433, \"lng\": -70.87562476858606}]', NULL, NULL),
(27, 32, 32, 'AVE3P32', 116, 32, 1, 69, '[{\"lat\": -34.20693340638778, \"lng\": -70.87730463647667}, {\"lat\": -34.20645092390403, \"lng\": -70.87580268562866}, {\"lat\": -34.2066049079755, \"lng\": -70.87572407112145}, {\"lat\": -34.20708738957784, \"lng\": -70.87724670999768}, {\"lat\": -34.20693340638778, \"lng\": -70.87730463647667}]', NULL, NULL),
(28, 31, 31, 'AVE3P31', 115, 31, 1, 70, '[{\"lat\": -34.20666650152534, \"lng\": -70.87743290225157}, {\"lat\": -34.20619428315977, \"lng\": -70.87591853858663}, {\"lat\": -34.206327736444294, \"lng\": -70.87585233689636}, {\"lat\": -34.206813641489944, \"lng\": -70.87735842535001}, {\"lat\": -34.20666650152534, \"lng\": -70.87743290225157}]', NULL, NULL),
(29, 30, 30, 'AVE3P30', 114, 30, 1, 71, '[{\"lat\": -34.20639275207049, \"lng\": -70.8775653056321}, {\"lat\": -34.20591711027825, \"lng\": -70.87601370351638}, {\"lat\": -34.206067673438085, \"lng\": -70.87594750182612}, {\"lat\": -34.206536470645126, \"lng\": -70.87748669112491}, {\"lat\": -34.20639275207049, \"lng\": -70.8775653056321}]', NULL, NULL),
(30, 29, 29, 'AVE3P29', 113, 29, 1, 72, '[{\"lat\": -34.206132689264855, \"lng\": -70.87767288337879}, {\"lat\": -34.20565020219759, \"lng\": -70.87615851971385}, {\"lat\": -34.20579734393636, \"lng\": -70.87608404281231}, {\"lat\": -34.20629009579571, \"lng\": -70.87759840647725}, {\"lat\": -34.206132689264855, \"lng\": -70.87767288337879}]', NULL, NULL),
(31, 28, 28, 'AVE3P28', 112, 28, 1, 73, '[{\"lat\": -34.205869203761914, \"lng\": -70.87778873633677}, {\"lat\": -34.20537987135698, \"lng\": -70.87628264788312}, {\"lat\": -34.205537279293665, \"lng\": -70.87622058379849}, {\"lat\": -34.20601634511836, \"lng\": -70.87771839704085}, {\"lat\": -34.205869203761914, \"lng\": -70.87778873633677}]', NULL, NULL),
(32, 27, 27, 'AVE3P27', 111, 27, 1, 74, '[{\"lat\": -34.205578342185326, \"lng\": -70.87791286450606}, {\"lat\": -34.20511296157513, \"lng\": -70.87642332647496}, {\"lat\": -34.205290901523604, \"lng\": -70.87634884957342}, {\"lat\": -34.205742593551925, \"lng\": -70.87788803887219}, {\"lat\": -34.205578342185326, \"lng\": -70.87791286450606}]', NULL, NULL),
(33, 33, 1, 'CERESMP01', 0, 33, 2, 75, '[{\"lat\": -34.32401031052249, \"lng\": -70.91739973855732}, {\"lat\": -34.32381094816333, \"lng\": -70.91633758378742}, {\"lat\": -34.323915059676644, \"lng\": -70.91629466844319}, {\"lat\": -34.324114421788465, \"lng\": -70.91736755204914}, {\"lat\": -34.32401474079177, \"lng\": -70.9173970563483}]', NULL, NULL),
(34, 34, 2, 'CERESMP02', 0, 34, 2, 76, '[{\"lat\": -34.3235871573018, \"lng\": -70.9171082254224}, {\"lat\": -34.3233855787862, \"lng\": -70.91603534181644}, {\"lat\": -34.32349855142052, \"lng\": -70.91599779089023}, {\"lat\": -34.32369348423583, \"lng\": -70.91708944995929}]', NULL, NULL),
(35, 35, 3, 'CERESMP03', 0, 35, 2, 77, '[{\"lat\": -34.32315520274689, \"lng\": -70.91681586463977}, {\"lat\": -34.32295583835627, \"lng\": -70.91573225219776}, {\"lat\": -34.32307324188809, \"lng\": -70.91569201906253}, {\"lat\": -34.3232703908456, \"lng\": -70.91679172475864}]', NULL, NULL),
(36, 36, 4, 'CERESMP04', 0, 36, 2, 78, '[{\"lat\": -34.32274162664212, \"lng\": -70.916520702941}, {\"lat\": -34.32254004609552, \"lng\": -70.91544513712603}, {\"lat\": -34.322648589526935, \"lng\": -70.91540222178179}, {\"lat\": -34.32285016981282, \"lng\": -70.91649656305987}]', NULL, NULL),
(37, 37, 5, 'CERESMP05', 0, 37, 2, 79, '[{\"lat\": -34.32231852845294, \"lng\": -70.91621761332232}, {\"lat\": -34.32212137725928, \"lng\": -70.91513668308932}, {\"lat\": -34.32222770605048, \"lng\": -70.91511790762621}, {\"lat\": -34.32243150252371, \"lng\": -70.9161961556502}]', NULL, NULL),
(38, 38, 6, 'CERESMP06', 0, 38, 2, 80, '[{\"lat\": -34.32189610069282, \"lng\": -70.91592797574879}, {\"lat\": -34.32170559409362, \"lng\": -70.91485240993381}, {\"lat\": -34.32181192341161, \"lng\": -70.9148148590076}, {\"lat\": -34.322002429769434, \"lng\": -70.91590383586765}]', NULL, NULL),
(39, 39, 7, 'CERESMP07', 0, 39, 2, 81, '[{\"lat\": -34.32147502540514, \"lng\": -70.91563308886549}, {\"lat\": -34.32128008743673, \"lng\": -70.91456020525953}, {\"lat\": -34.32138420208984, \"lng\": -70.91451997212431}, {\"lat\": -34.32158800061141, \"lng\": -70.91560894898436}]', NULL, NULL),
(40, 40, 8, 'CERESMP08', 0, 40, 2, 82, '[{\"lat\": -34.32105192083037, \"lng\": -70.91534341029188}, {\"lat\": -34.320856981879174, \"lng\": -70.91425443343184}, {\"lat\": -34.320974388347196, \"lng\": -70.91420883587858}, {\"lat\": -34.321162681396366, \"lng\": -70.91531390599272}]', NULL, NULL),
(41, 41, 9, 'CERESMP09', 0, 41, 2, 83, '[{\"lat\": -34.320635459794126, \"lng\": -70.91504836730024}, {\"lat\": -34.320438304646665, \"lng\": -70.91396475485823}, {\"lat\": -34.32054685079638, \"lng\": -70.91393256835005}, {\"lat\": -34.32074622090974, \"lng\": -70.91501886300108}]', NULL, NULL),
(42, 42, 10, 'CERESMP10', 0, 42, 2, 84, '[{\"lat\": -34.32019905957607, \"lng\": -70.9147533243086}, {\"lat\": -34.32001962532557, \"lng\": -70.91366971186659}, {\"lat\": -34.32013481772799, \"lng\": -70.91364557198546}, {\"lat\": -34.32032532789275, \"lng\": -70.91472650221846}]', NULL, NULL),
(43, 43, 43, 'CERESMP100', 0, 43, 2, 85, NULL, NULL, NULL),
(44, 44, 1, 'CERESMP100', 0, 43, 3, 86, NULL, NULL, NULL),
(45, 45, 2, 'CERESMP100', 0, 43, 3, 86, NULL, NULL, NULL),
(46, 46, 3, 'CERESMP100', 0, 43, 3, 86, NULL, NULL, NULL),
(47, 47, 4, 'CERESMP100', 0, 43, 3, 86, NULL, NULL, NULL),
(48, 48, 5, 'CERESMP100', 0, 43, 3, 86, NULL, NULL, NULL),
(49, 49, 6, 'CERESMP100', 0, 43, 3, 86, NULL, NULL, NULL),
(50, 50, 7, 'CERESMP100', 0, 43, 3, 86, NULL, NULL, NULL),
(51, 51, 8, 'CERESMP100', 0, 43, 3, 86, NULL, NULL, NULL),
(52, 52, 9, 'CERESMP100', 0, 43, 3, 86, NULL, NULL, NULL),
(53, 53, 10, 'CERESMP100', 0, 43, 3, 86, NULL, NULL, NULL),
(54, 54, 11, 'CERESMP100', 0, 43, 3, 86, NULL, NULL, NULL),
(55, 55, 12, 'CERESMP100', 0, 43, 3, 86, NULL, NULL, NULL),
(56, 56, 13, 'CERESMP100', 0, 43, 3, 86, NULL, NULL, NULL),
(57, 57, 14, 'CERESMP100', 0, 43, 3, 86, NULL, NULL, NULL),
(58, 58, 15, 'CERESMP100', 0, 43, 3, 86, NULL, NULL, NULL),
(59, 59, 16, 'CERESMP100', 0, 43, 3, 86, NULL, NULL, NULL),
(60, 60, 17, 'CERESMP100', 0, 43, 3, 86, NULL, NULL, NULL),
(61, 61, 18, 'CERESMP100', 0, 43, 3, 86, NULL, NULL, NULL),
(62, 62, 19, 'CERESMP100', 0, 43, 3, 86, NULL, NULL, NULL),
(63, 63, 20, 'CERESMP100', 0, 43, 3, 86, NULL, NULL, NULL),
(64, 64, 21, 'CERESMP100', 0, 43, 3, 86, NULL, NULL, NULL),
(65, 65, 22, 'CERESMP100', 0, 43, 3, 86, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pais`
--

CREATE TABLE `pais` (
  `id` int(11) NOT NULL,
  `pais` varchar(50) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `pais`
--

INSERT INTO `pais` (`id`, `pais`) VALUES
(1, 'Chile');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pesaje_aves_historico`
--

CREATE TABLE `pesaje_aves_historico` (
  `id` int(11) NOT NULL,
  `edad` int(11) DEFAULT NULL,
  `historico0` int(11) DEFAULT NULL,
  `historico1` int(11) DEFAULT NULL,
  `historico2` int(11) DEFAULT NULL,
  `historico3` int(11) DEFAULT NULL,
  `historico4` int(11) DEFAULT NULL,
  `historico5` int(11) DEFAULT NULL,
  `historico6` int(11) DEFAULT NULL,
  `historico7` int(11) DEFAULT NULL,
  `historico8` int(11) DEFAULT NULL,
  `historico9` int(11) DEFAULT NULL,
  `historico10` int(11) DEFAULT NULL,
  `historico11` int(11) DEFAULT NULL,
  `historico12` int(11) DEFAULT NULL,
  `historico13` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `peso_silo`
--

CREATE TABLE `peso_silo` (
  `id` int(11) NOT NULL,
  `numero_serie` varchar(16) COLLATE utf8_bin NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `peso_total_silo1` float DEFAULT NULL,
  `peso_total_silo2` int(11) DEFAULT NULL,
  `peso_suavizado_silo1` float DEFAULT NULL,
  `peso_suavizado_silo2` int(11) DEFAULT NULL,
  `peso_pata1` float DEFAULT NULL,
  `peso_pata2` float DEFAULT NULL,
  `peso_pata3` float DEFAULT NULL,
  `peso_pata4` float DEFAULT NULL,
  `peso_pata5` float DEFAULT NULL,
  `peso_pata6` float DEFAULT NULL,
  `status_celda` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `peso_silo_1_respaldo`
--

CREATE TABLE `peso_silo_1_respaldo` (
  `id` int(11) DEFAULT NULL,
  `numero_serie` tinytext COLLATE utf8_bin,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `peso_total_silo1` double DEFAULT NULL,
  `peso_total_silo2` int(11) DEFAULT NULL,
  `peso_suavizado_silo1` double DEFAULT NULL,
  `peso_suavizado_silo2` int(11) DEFAULT NULL,
  `peso_pata1` double DEFAULT NULL,
  `peso_pata2` double DEFAULT NULL,
  `peso_pata3` double DEFAULT NULL,
  `peso_pata4` double DEFAULT NULL,
  `peso_pata5` double DEFAULT NULL,
  `peso_pata6` double DEFAULT NULL,
  `status_celda` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `peso_silo_diario`
--

CREATE TABLE `peso_silo_diario` (
  `id` int(6) UNSIGNED NOT NULL,
  `id_pabellon` int(11) NOT NULL,
  `nro_silo` int(11) NOT NULL,
  `peso` double DEFAULT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `plantas`
--

CREATE TABLE `plantas` (
  `id` int(11) NOT NULL,
  `id_area_productiva` int(11) NOT NULL,
  `planta` varchar(50) COLLATE utf8_bin NOT NULL,
  `cod_area_productiva` varchar(50) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `proceso`
--

CREATE TABLE `proceso` (
  `id` int(11) NOT NULL,
  `id_instalacion` int(11) NOT NULL,
  `proceso` varchar(50) COLLATE utf8_bin NOT NULL,
  `cod_proceso` varchar(50) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `region`
--

CREATE TABLE `region` (
  `id` int(11) NOT NULL,
  `id_pais` int(11) NOT NULL,
  `region` varchar(50) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `region`
--

INSERT INTO `region` (`id`, `id_pais`, `region`) VALUES
(1, 1, 'XV de Arica y Parinacota'),
(2, 1, 'I de Tarapacá'),
(3, 1, 'II de Antofagasta'),
(4, 1, 'III de Atacama'),
(5, 1, 'IV de Coquimbo'),
(6, 1, 'V de Valparaíso'),
(7, 1, 'VI del Libertador General Bernardo O\'Higgins'),
(8, 1, 'VII del Maule'),
(9, 1, 'VIII del Bío Bío'),
(10, 1, 'IX de la Araucanía'),
(11, 1, 'XIV de los Ríos'),
(12, 1, 'X de los Lagos'),
(13, 1, 'XI Aisén del General Carlos Ibáñez del Campo'),
(14, 1, 'XII de Magallanes y Antártica Chilena'),
(15, 1, 'XIII Metropolitana de Santiago');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `registro_cerdos`
--

CREATE TABLE `registro_cerdos` (
  `id` int(11) NOT NULL,
  `id_pabellon` int(11) DEFAULT NULL,
  `id_origen` int(11) DEFAULT NULL,
  `id_crianza_historico` int(11) DEFAULT NULL,
  `id_usuario` int(11) DEFAULT NULL,
  `hora_inicio_crianza` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `cantidad_inicial_animales` float DEFAULT NULL,
  `sexo` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `peso_promedio_inicial` float DEFAULT NULL,
  `edad_inicio` float DEFAULT NULL,
  `edad_reproductora` float DEFAULT NULL,
  `cantidad_nuevos` int(11) DEFAULT NULL,
  `cantidad_medianos` int(11) DEFAULT NULL,
  `cantidad_viejos` int(11) DEFAULT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `numero_crianza` int(11) DEFAULT NULL,
  `fecha_inicio_crianza` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `registro_mortalidad`
--

CREATE TABLE `registro_mortalidad` (
  `id` int(11) NOT NULL,
  `pabellon_id` int(11) DEFAULT NULL,
  `fecha_registro` datetime DEFAULT NULL,
  `causa_id` int(11) DEFAULT NULL,
  `cantidad` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `registro_mortalidad`
--

INSERT INTO `registro_mortalidad` (`id`, `pabellon_id`, `fecha_registro`, `causa_id`, `cantidad`) VALUES
(2, 3, '2019-03-19 13:54:00', 5, 2),
(3, 3, '2019-03-19 13:54:00', 9, 3),
(4, 2, '2019-03-19 14:06:00', 5, 1),
(5, 2, '2019-03-19 14:06:00', 9, 1),
(6, 3, '2019-01-19 14:08:00', 5, 1),
(7, 4, '2019-03-19 14:57:00', 4, 3),
(8, 20, '2019-03-19 15:02:00', 3, 1),
(9, 20, '2019-03-19 15:02:00', 8, 3),
(10, 6, '2019-03-19 15:30:00', 3, 1),
(11, 6, '2019-03-19 15:30:00', 8, 4),
(12, 15, '2019-01-20 12:04:00', 12, 3),
(13, 15, '2019-01-20 12:04:00', 19, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sector`
--

CREATE TABLE `sector` (
  `id` int(11) NOT NULL,
  `id_zona` int(11) NOT NULL,
  `sector` varchar(50) COLLATE utf8_bin NOT NULL,
  `numero_sector` varchar(50) COLLATE utf8_bin NOT NULL,
  `centro_costo` varchar(50) COLLATE utf8_bin NOT NULL,
  `poligono` varchar(500) COLLATE utf8_bin DEFAULT NULL,
  `nombre_corto` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `username` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `animal` varchar(100) COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `sector`
--

INSERT INTO `sector` (`id`, `id_zona`, `sector`, `numero_sector`, `centro_costo`, `poligono`, `nombre_corto`, `username`, `animal`) VALUES
(1, 1, 'Bosque Viejo', '3', 'C001005003', '[{\"lat\": -34.205530435476454, \"lng\": -70.87810733197121}, {\"lat\": -34.202738111687566, \"lng\": -70.8695921395602}, {\"lat\": -34.20484605094812, \"lng\": -70.86864049026255}, {\"lat\": -34.20598212622118, \"lng\": -70.87202505167762}, {\"lat\": -34.205674155521415, \"lng\": -70.87223193195972}, {\"lat\": -34.20727', 'Bosque Viejo', 'aves', 'aves'),
(2, 2, 'La Esmeralda Cerdos Engordas', '1', 'C001106006', '[{\"lat\": -34.205530435476454, \"lng\": -70.87810733197121}, {\"lat\": -34.202738111687566, \"lng\": -70.8695921395602}, {\"lat\": -34.20484605094812, \"lng\": -70.86864049026255}, {\"lat\": -34.20598212622118, \"lng\": -70.87202505167762}, {\"lat\": -34.205674155521415, \"lng\": -70.87223193195972}, {\"lat\": -34.20727', 'La Esmeralda', 'cengordale', 'cerdos'),
(3, 3, 'El Mirador', '4', 'aaa', NULL, 'El Mirador', 'mirador', 'cerdos');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `silo`
--

CREATE TABLE `silo` (
  `id` int(11) NOT NULL,
  `id_pabellon` int(11) NOT NULL,
  `silo` varchar(50) COLLATE utf8_bin NOT NULL,
  `capacidad` int(11) NOT NULL,
  `marca` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `modelo` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `tipo` varchar(10) COLLATE utf8_bin DEFAULT NULL,
  `estado` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `silo`
--

INSERT INTO `silo` (`id`, `id_pabellon`, `silo`, `capacidad`, `marca`, `modelo`, `tipo`, `estado`) VALUES
(5, 1, 'Silo 1', 13000, 'ChoreTime', 'Modelo S', 'Tandem', 1),
(6, 1, 'Silo 2', 13000, 'Por Ingresar', 'Por Ingresar', 'Tandem', 1),
(7, 2, 'Silo 1', 13000, 'Por Ingresar', 'Por Ingresar', 'Serie', 1),
(8, 2, 'Silo 2', 13000, 'Por Ingresar', 'Por Ingresar', 'Serie', 1),
(9, 3, 'Silo 1', 13000, 'Por Ingresar', 'Por Ingresar', 'Tandem', 1),
(10, 3, 'Silo 2', 13000, 'Por Ingresar', 'Por Ingresar', 'Tandem', 1),
(11, 4, 'Silo 1', 13000, 'Por Ingresar', 'Por Ingresar', 'Serie', 1),
(12, 4, 'Silo 2', 13000, 'Por Ingresar', 'Por Ingresar', 'Serie', 1),
(13, 5, 'Silo 1', 13000, 'Por Ingresar', 'Por Ingresar', 'Tandem', 1),
(14, 5, 'Silo 2', 13000, 'Por Ingresar', 'Por Ingresar', 'Tandem', 1),
(15, 6, 'Silo 1', 13000, 'Por Ingresar', 'Por Ingresar', 'Serie', 1),
(16, 6, 'Silo 2', 13000, 'Por Ingresar', 'Por Ingresar', 'Serie', 1),
(17, 7, 'Silo 1', 13000, 'Por Ingresar', 'Por Ingresar', 'Tandem', 1),
(18, 7, 'Silo 2', 13000, 'Por Ingresar', 'Por Ingresar', 'Tandem', 1),
(19, 8, 'Silo 1', 13000, 'Por Ingresar', 'Por Ingresar', 'Serie', 1),
(20, 8, 'Silo 2', 13000, 'Por Ingresar', 'Por Ingresar', 'Serie', 1),
(21, 9, 'Silo 1', 13000, 'Por Ingresar', 'Por Ingresar', 'Tandem', 1),
(22, 9, 'Silo 2', 13000, 'Por Ingresar', 'Por Ingresar', 'Tandem', 1),
(23, 10, 'Silo 1', 13000, 'Por Ingresar', 'Por Ingresar', 'Serie', 1),
(24, 10, 'Silo 2', 13000, 'Por Ingresar', 'Por Ingresar', 'Serie', 1),
(25, 11, 'Silo 1', 13000, 'Por Ingresar', 'Por Ingresar', 'Tandem', 1),
(26, 11, 'Silo 2', 13000, 'Por Ingresar', 'Por Ingresar', 'Tandem', 1),
(27, 12, 'Silo 1', 13000, 'Por Ingresar', 'Por Ingresar', 'Serie', 1),
(28, 12, 'Silo 2', 13000, 'Por Ingresar', 'Por Ingresar', 'Serie', 1),
(29, 13, 'Silo 1', 13000, 'Por Ingresar', 'Por Ingresar', 'Tandem', 1),
(30, 13, 'Silo 2', 13000, 'Por Ingresar', 'Por Ingresar', 'Tandem', 1),
(31, 14, 'Silo 1', 13000, 'Por Ingresar', 'Por Ingresar', 'Serie', 1),
(32, 14, 'Silo 2', 13000, 'Por Ingresar', 'Por Ingresar', 'Serie', 1),
(33, 15, 'Silo 1', 13000, 'Por Ingresar', 'Por Ingresar', 'Tandem', 1),
(34, 15, 'Silo 2', 13000, 'Por Ingresar', 'Por Ingresar', 'Tandem', 1),
(35, 16, 'Silo 1', 13000, 'Por Ingresar', 'Por Ingresar', 'Serie', 1),
(36, 16, 'Silo 2', 13000, 'Por Ingresar', 'Por Ingresar', 'Serie', 1),
(37, 17, 'Silo 1', 13000, 'Por Ingresar', 'Por Ingresar', 'Tandem', 1),
(38, 17, 'Silo 2', 13000, 'Por Ingresar', 'Por Ingresar', 'Tandem', 1),
(39, 18, 'Silo 1', 13000, 'Por Ingresar', 'Por Ingresar', 'Serie', 1),
(40, 18, 'Silo 2', 13000, 'Por Ingresar', 'Por Ingresar', 'Serie', 1),
(41, 19, 'Silo 1', 13000, 'Por Ingresar', 'Por Ingresar', 'Tandem', 1),
(42, 19, 'Silo 2', 13000, 'Por Ingresar', 'Por Ingresar', 'Tandem', 1),
(43, 20, 'Silo 1', 13000, 'Por Ingresar', 'Por Ingresar', 'Serie', 1),
(44, 20, 'Silo 2', 13000, 'Por Ingresar', 'Por Ingresar', 'Serie', 1),
(45, 21, 'Silo 1', 13000, 'Por Ingresar', 'Por Ingresar', 'Serie', 1),
(46, 21, 'Silo 2', 13000, 'Por Ingresar', 'Por Ingresar', 'Serie', 1),
(47, 22, 'Silo 1', 13000, 'Por Ingresar', 'Por Ingresar', 'Serie', 1),
(48, 22, 'Silo 2', 13000, 'Por Ingresar', 'Por Ingresar', 'Serie', 1),
(49, 23, 'Silo 1', 13000, 'Por Ingresar', 'Por Ingresar', 'Serie', 1),
(50, 23, 'Silo 2', 13000, 'Por Ingresar', 'Por Ingresar', 'Serie', 1),
(51, 24, 'Silo 1', 13000, 'Por Ingresar', 'Por Ingresar', 'Serie', 1),
(52, 24, 'Silo 2', 13000, 'Por Ingresar', 'Por Ingresar', 'Serie', 1),
(53, 25, 'Silo 1', 13000, 'Por Ingresar', 'Por Ingresar', 'Serie', 1),
(54, 25, 'Silo 2', 13000, 'Por Ingresar', 'Por Ingresar', 'Serie', 1),
(55, 26, 'Silo 1', 13000, 'Por Ingresar', 'Por Ingresar', 'Serie', 1),
(56, 26, 'Silo 2', 13000, 'Por Ingresar', 'Por Ingresar', 'Serie', 1),
(57, 27, 'Silo 1', 13000, 'Por Ingresar', 'Por Ingresar', 'Tandem', 1),
(58, 27, 'Silo 2', 13000, 'Por Ingresar', 'Por Ingresar', 'Tandem', 1),
(59, 28, 'Silo 1', 13000, 'Por Ingresar', 'Por Ingresar', 'Tandem', 1),
(60, 28, 'Silo 2', 13000, 'Por Ingresar', 'Por Ingresar', 'Tandem', 1),
(61, 29, 'Silo 1', 13000, 'Por Ingresar', 'Por Ingresar', 'Tandem', 1),
(62, 29, 'Silo 2', 13000, 'Por Ingresar', 'Por Ingresar', 'Tandem', 1),
(63, 30, 'Silo 1', 13000, 'Por Ingresar', 'Por Ingresar', 'Tandem', 1),
(64, 30, 'Silo 2', 13000, 'Por Ingresar', 'Por Ingresar', 'Tandem', 1),
(65, 31, 'Silo 1', 13000, 'Por Ingresar', 'Por Ingresar', 'Tandem', 1),
(66, 31, 'Silo 2', 13000, 'Por Ingresar', 'Por Ingresar', 'Tandem', 1),
(67, 32, 'Silo 1', 13000, 'Por Ingresar', 'Por Ingresar', 'Tandem', 1),
(68, 32, 'Silo 2', 13000, 'Por Ingresar', 'Por Ingresar', 'Tandem', 1),
(69, 33, 'Silo 1', 12000, 'Por Ingresar', 'Por Ingresar', 'Serie', 1),
(70, 33, 'Silo 2', 12000, 'Por Ingresar', 'Por Ingresar', 'Serie', 1),
(71, 34, 'Silo 1', 12000, 'Por Ingresar', 'Por Ingresar', 'Serie', 1),
(72, 34, 'Silo 2', 12000, 'Por Ingresar', 'Por Ingresar', 'Serie', 1),
(73, 35, 'Silo 1', 12000, 'Por Ingresar', 'Por Ingresar', 'Serie', 1),
(74, 35, 'Silo 2', 12000, 'Por Ingresar', 'Por Ingresar', 'Serie', 1),
(75, 36, 'Silo 1', 12000, 'Por Ingresar', 'Por Ingresar', 'Tandem', 1),
(76, 36, 'Silo 2', 12000, 'Por Ingresar', 'Por Ingresar', 'Tandem', 1),
(77, 37, 'Silo 1', 12000, 'Por Ingresar', 'Por Ingresar', 'Tandem', 1),
(78, 37, 'Silo 2', 12000, 'Por Ingresar', 'Por Ingresar', 'Tandem', 1),
(79, 38, 'Silo 1', 12000, 'Por Ingresar', 'Por Ingresar', 'Tandem', 1),
(80, 38, 'Silo 2', 12000, 'Por Ingresar', 'Por Ingresar', 'Tandem', 1),
(81, 39, 'Silo 1', 12000, 'Por Ingresar', 'Por Ingresar', 'Tandem', 1),
(82, 39, 'Silo 2', 12000, 'Por Ingresar', 'Por Ingresar', 'Tandem', 1),
(83, 40, 'Silo 1', 12000, 'Por Ingresar', 'Por Ingresar', 'Tandem', 1),
(84, 40, 'Silo 2', 12000, 'Por Ingresar', 'Por Ingresar', 'Tandem', 1),
(85, 41, 'Silo 1', 12000, 'Por Ingresar', 'Por Ingresar', 'Tandem', 1),
(86, 41, 'Silo 2', 12000, 'Por Ingresar', 'Por Ingresar', 'Tandem', 1),
(87, 42, 'Silo 1', 12000, 'Por Ingresar', 'Por Ingresar', 'Tandem', 1),
(88, 42, 'Silo 2', 12000, 'Por Ingresar', 'Por Ingresar', 'Tandem', 1),
(89, 43, 'Silo 1', 12000, 'Por Ingresar', 'Por Ingresar', 'Tandem', 1),
(90, 43, 'Silo 2', 12000, 'Por Ingresar', 'Por Ingresar', 'Tandem', 1),
(91, 44, 'Silo 1', 12000, 'Por Ingresar', 'Por Ingresar', 'Tandem', 1),
(92, 44, 'Silo 2', 12000, 'Por Ingresar', 'Por Ingresar', 'Tandem', 1),
(93, 45, 'Silo 1', 12000, 'Por Ingresar', 'Por Ingresar', 'Tandem', 1),
(94, 45, 'Silo 2', 12000, 'Por Ingresar', 'Por Ingresar', 'Tandem', 1),
(95, 46, 'Silo 1', 12000, 'Por Ingresar', 'Por Ingresar', 'Tandem', 1),
(96, 46, 'Silo 2', 12000, 'Por Ingresar', 'Por Ingresar', 'Tandem', 1),
(97, 47, 'Silo 1', 12000, 'Por Ingresar', 'Por Ingresar', 'Tandem', 1),
(98, 47, 'Silo 2', 12000, 'Por Ingresar', 'Por Ingresar', 'Tandem', 1),
(99, 48, 'Silo 1', 12000, 'Por Ingresar', 'Por Ingresar', 'Tandem', 1),
(100, 48, 'Silo 2', 12000, 'Por Ingresar', 'Por Ingresar', 'Tandem', 1),
(101, 49, 'Silo 1', 12000, 'Por Ingresar', 'Por Ingresar', 'Tandem', 1),
(102, 49, 'Silo 2', 12000, 'Por Ingresar', 'Por Ingresar', 'Tandem', 1),
(103, 50, 'Silo 1', 12000, 'Por Ingresar', 'Por Ingresar', 'Tandem', 1),
(104, 50, 'Silo 2', 12000, 'Por Ingresar', 'Por Ingresar', 'Tandem', 1),
(105, 51, 'Silo 1', 12000, 'Por Ingresar', 'Por Ingresar', 'Tandem', 1),
(106, 51, 'Silo 2', 12000, 'Por Ingresar', 'Por Ingresar', 'Tandem', 1),
(107, 52, 'Silo 1', 12000, 'Por Ingresar', 'Por Ingresar', 'Tandem', 1),
(108, 52, 'Silo 2', 12000, 'Por Ingresar', 'Por Ingresar', 'Tandem', 1),
(109, 53, 'Silo 1', 12000, 'Por Ingresar', 'Por Ingresar', 'Tandem', 1),
(110, 53, 'Silo 2', 12000, 'Por Ingresar', 'Por Ingresar', 'Tandem', 1),
(111, 54, 'Silo 1', 12000, 'Por Ingresar', 'Por Ingresar', 'Tandem', 1),
(112, 54, 'Silo 2', 12000, 'Por Ingresar', 'Por Ingresar', 'Tandem', 1),
(113, 55, 'Silo 1', 12000, 'Por Ingresar', 'Por Ingresar', 'Tandem', 1),
(114, 55, 'Silo 2', 12000, 'Por Ingresar', 'Por Ingresar', 'Tandem', 1),
(115, 56, 'Silo 1', 12000, 'Por Ingresar', 'Por Ingresar', 'Tandem', 1),
(116, 56, 'Silo 2', 12000, 'Por Ingresar', 'Por Ingresar', 'Tandem', 1),
(117, 57, 'Silo 1', 12000, 'Por Ingresar', 'Por Ingresar', 'Tandem', 1),
(118, 57, 'Silo 2', 12000, 'Por Ingresar', 'Por Ingresar', 'Tandem', 1),
(119, 58, 'Silo 1', 12000, 'Por Ingresar', 'Por Ingresar', 'Tandem', 1),
(120, 58, 'Silo 2', 12000, 'Por Ingresar', 'Por Ingresar', 'Tandem', 1),
(121, 59, 'Silo 1', 12000, 'Por Ingresar', 'Por Ingresar', 'Tandem', 1),
(122, 59, 'Silo 2', 12000, 'Por Ingresar', 'Por Ingresar', 'Tandem', 1),
(123, 60, 'Silo 1', 12000, 'Por Ingresar', 'Por Ingresar', 'Tandem', 1),
(124, 60, 'Silo 2', 12000, 'Por Ingresar', 'Por Ingresar', 'Tandem', 1),
(125, 61, 'Silo 1', 12000, 'Por Ingresar', 'Por Ingresar', 'Tandem', 1),
(126, 61, 'Silo 2', 12000, 'Por Ingresar', 'Por Ingresar', 'Tandem', 1),
(127, 62, 'Silo 1', 12000, 'Por Ingresar', 'Por Ingresar', 'Tandem', 1),
(128, 62, 'Silo 2', 12000, 'Por Ingresar', 'Por Ingresar', 'Tandem', 1),
(129, 63, 'Silo 1', 12000, 'Por Ingresar', 'Por Ingresar', 'Tandem', 1),
(130, 63, 'Silo 2', 12000, 'Por Ingresar', 'Por Ingresar', 'Tandem', 1),
(131, 64, 'Silo 1', 12000, 'Por Ingresar', 'Por Ingresar', 'Tandem', 1),
(132, 64, 'Silo 2', 12000, 'Por Ingresar', 'Por Ingresar', 'Tandem', 1),
(133, 65, 'Silo 1', 12000, 'Por Ingresar', 'Por Ingresar', 'Tandem', 1),
(134, 65, 'Silo 2', 12000, 'Por Ingresar', 'Por Ingresar', 'Tandem', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `temperatura_cerdos`
--

CREATE TABLE `temperatura_cerdos` (
  `id` int(11) NOT NULL,
  `edad` int(11) DEFAULT NULL,
  `temperatura_deseada` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `temperatura_deseada`
--

CREATE TABLE `temperatura_deseada` (
  `id` int(11) NOT NULL,
  `edad` int(11) NOT NULL,
  `temperatura_deseada` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_instalacion`
--

CREATE TABLE `tipo_instalacion` (
  `id` int(11) NOT NULL,
  `instalacion` varchar(50) COLLATE utf8_bin NOT NULL,
  `cod_instalacion` varchar(50) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE `usuario` (
  `username` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `password` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `email` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `rut` varchar(100) COLLATE utf8_bin NOT NULL,
  `nombre` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `apellido` varchar(100) COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`username`, `password`, `email`, `rut`, `nombre`, `apellido`) VALUES
('cengordale', 'agrosuper4.0', 'cengordale@agrosuper.com', '11111111-1', 'cengordale', NULL),
('aves', 'agrosuper4.0', 'aves@agrosuper.com', '22222222-1', 'aves', NULL),
('mirador', 'agrosuper4.0', 'mirador@agrosuper.com', '33333333-1', 'mirador', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `weather_underground`
--

CREATE TABLE `weather_underground` (
  `id` int(11) NOT NULL,
  `timestamp` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `temperatura_exterior` float DEFAULT NULL,
  `humedad_exterior` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `zona`
--

CREATE TABLE `zona` (
  `id` int(11) NOT NULL,
  `id_macrozona` int(11) NOT NULL,
  `zona` varchar(50) COLLATE utf8_bin NOT NULL,
  `cod_zona` varchar(20) COLLATE utf8_bin NOT NULL,
  `poligono` varchar(500) COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `zona`
--

INSERT INTO `zona` (`id`, `id_macrozona`, `zona`, `cod_zona`, `poligono`) VALUES
(1, 1, 'Lo Miranda', '1', '[{\"lat\": -34.204770768221884, \"lng\": -70.87961342042485}, {\"lat\": -34.2090275531178, \"lng\": -70.8777763235199}, {\"lat\": -34.20084229102258, \"lng\": -70.85114669360858}, {\"lat\": -34.196489265532016, \"lng\": -70.85296724009103}, {\"lat\": -34.204770768221884, \"lng\": -70.87961342042485}]'),
(2, 1, 'Requinoa - Rosario', '2', NULL),
(3, 1, 'Mirador', '3', NULL);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `area_productiva`
--
ALTER TABLE `area_productiva`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_proceso` (`id_proceso`);

--
-- Indices de la tabla `bienestar_equipo`
--
ALTER TABLE `bienestar_equipo`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_equipo` (`id_equipo`);

--
-- Indices de la tabla `bienestar_equipo_choretime`
--
ALTER TABLE `bienestar_equipo_choretime`
  ADD PRIMARY KEY (`id_equipo`);

--
-- Indices de la tabla `cargo`
--
ALTER TABLE `cargo`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `cobb500_pollos`
--
ALTER TABLE `cobb500_pollos`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `cobb500_pollos_id_uindex` (`id`);

--
-- Indices de la tabla `crianza`
--
ALTER TABLE `crianza`
  ADD PRIMARY KEY (`id`),
  ADD KEY `crianza__id_fk` (`id_sector`);

--
-- Indices de la tabla `crianza_pabellon`
--
ALTER TABLE `crianza_pabellon`
  ADD PRIMARY KEY (`id`),
  ADD KEY `crianza_pabellon___fk` (`id_pabellon`);

--
-- Indices de la tabla `detalle_dieta_sectores`
--
ALTER TABLE `detalle_dieta_sectores`
  ADD PRIMARY KEY (`id`),
  ADD KEY `detalle_dieta_sectores___fk` (`id_sector`),
  ADD KEY `detalle_dieta_sectores___fk_2` (`id_dieta`);

--
-- Indices de la tabla `dieta`
--
ALTER TABLE `dieta`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `dieta_historico`
--
ALTER TABLE `dieta_historico`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_dieta` (`id_dieta`),
  ADD KEY `dieta_historico___fk` (`id_guia`);

--
-- Indices de la tabla `equipos`
--
ALTER TABLE `equipos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_pabellon` (`id_pabellon`),
  ADD KEY `id_tipo` (`id_tipo_equipo`);

--
-- Indices de la tabla `equipo_tipo`
--
ALTER TABLE `equipo_tipo`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `estandar_cerdos`
--
ALTER TABLE `estandar_cerdos`
  ADD PRIMARY KEY (`id_estandar`);

--
-- Indices de la tabla `gerencias`
--
ALTER TABLE `gerencias`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `guias_despacho`
--
ALTER TABLE `guias_despacho`
  ADD PRIMARY KEY (`id`),
  ADD KEY `dieta_fk` (`dieta_id`),
  ADD KEY `sectors_fk` (`sector_id`);

--
-- Indices de la tabla `itr`
--
ALTER TABLE `itr`
  ADD PRIMARY KEY (`identificador`);

--
-- Indices de la tabla `itr_bk`
--
ALTER TABLE `itr_bk`
  ADD PRIMARY KEY (`identificador`);

--
-- Indices de la tabla `kpi_cerdos`
--
ALTER TABLE `kpi_cerdos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `kpi_diarios`
--
ALTER TABLE `kpi_diarios`
  ADD PRIMARY KEY (`id`),
  ADD KEY `kpi_diarios2_pabellon_id_fk` (`id_pabellon`);

--
-- Indices de la tabla `macrozona`
--
ALTER TABLE `macrozona`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_region` (`id_region`);

--
-- Indices de la tabla `medicion_pabellon`
--
ALTER TABLE `medicion_pabellon`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_equipo` (`id_equipo`);

--
-- Indices de la tabla `modelos_crecimiento_aves`
--
ALTER TABLE `modelos_crecimiento_aves`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `mortalidad`
--
ALTER TABLE `mortalidad`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `pabellon`
--
ALTER TABLE `pabellon`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_equipo_chore_time` (`id_equipo_chore_time`),
  ADD KEY `id_equipo_gases` (`id_equipo_gases`),
  ADD KEY `id_equipo_silo` (`id_equipo_silo`),
  ADD KEY `id_sector` (`id_sector`);

--
-- Indices de la tabla `pais`
--
ALTER TABLE `pais`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `pesaje_aves_historico`
--
ALTER TABLE `pesaje_aves_historico`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `peso_silo`
--
ALTER TABLE `peso_silo`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_silo` (`numero_serie`);

--
-- Indices de la tabla `peso_silo_diario`
--
ALTER TABLE `peso_silo_diario`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `plantas`
--
ALTER TABLE `plantas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_area_productiva` (`id_area_productiva`);

--
-- Indices de la tabla `proceso`
--
ALTER TABLE `proceso`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_instalacion` (`id_instalacion`);

--
-- Indices de la tabla `region`
--
ALTER TABLE `region`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_pais` (`id_pais`);

--
-- Indices de la tabla `registro_cerdos`
--
ALTER TABLE `registro_cerdos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `registro_mortalidad`
--
ALTER TABLE `registro_mortalidad`
  ADD PRIMARY KEY (`id`),
  ADD KEY `registro_mortalidad___fk` (`pabellon_id`),
  ADD KEY `registro_mortalidad___fk_2` (`causa_id`);

--
-- Indices de la tabla `sector`
--
ALTER TABLE `sector`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_zona` (`id_zona`),
  ADD KEY `sector___fk` (`username`);

--
-- Indices de la tabla `silo`
--
ALTER TABLE `silo`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_pabellon` (`id_pabellon`);

--
-- Indices de la tabla `temperatura_cerdos`
--
ALTER TABLE `temperatura_cerdos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `temperatura_deseada`
--
ALTER TABLE `temperatura_deseada`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tipo_instalacion`
--
ALTER TABLE `tipo_instalacion`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`rut`),
  ADD UNIQUE KEY `usuario_pk` (`username`),
  ADD UNIQUE KEY `usuario_pk_username` (`username`);

--
-- Indices de la tabla `weather_underground`
--
ALTER TABLE `weather_underground`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `zona`
--
ALTER TABLE `zona`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_macrozona` (`id_macrozona`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `bienestar_equipo`
--
ALTER TABLE `bienestar_equipo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `cargo`
--
ALTER TABLE `cargo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `cobb500_pollos`
--
ALTER TABLE `cobb500_pollos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `crianza`
--
ALTER TABLE `crianza`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `crianza_pabellon`
--
ALTER TABLE `crianza_pabellon`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT de la tabla `detalle_dieta_sectores`
--
ALTER TABLE `detalle_dieta_sectores`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT de la tabla `dieta`
--
ALTER TABLE `dieta`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT de la tabla `dieta_historico`
--
ALTER TABLE `dieta_historico`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `equipos`
--
ALTER TABLE `equipos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `equipo_tipo`
--
ALTER TABLE `equipo_tipo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `estandar_cerdos`
--
ALTER TABLE `estandar_cerdos`
  MODIFY `id_estandar` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `gerencias`
--
ALTER TABLE `gerencias`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `guias_despacho`
--
ALTER TABLE `guias_despacho`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `itr`
--
ALTER TABLE `itr`
  MODIFY `identificador` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `itr_bk`
--
ALTER TABLE `itr_bk`
  MODIFY `identificador` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `kpi_cerdos`
--
ALTER TABLE `kpi_cerdos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `kpi_diarios`
--
ALTER TABLE `kpi_diarios`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `macrozona`
--
ALTER TABLE `macrozona`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `medicion_pabellon`
--
ALTER TABLE `medicion_pabellon`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `modelos_crecimiento_aves`
--
ALTER TABLE `modelos_crecimiento_aves`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `mortalidad`
--
ALTER TABLE `mortalidad`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;

--
-- AUTO_INCREMENT de la tabla `pabellon`
--
ALTER TABLE `pabellon`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=66;

--
-- AUTO_INCREMENT de la tabla `pais`
--
ALTER TABLE `pais`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `pesaje_aves_historico`
--
ALTER TABLE `pesaje_aves_historico`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `peso_silo`
--
ALTER TABLE `peso_silo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `peso_silo_diario`
--
ALTER TABLE `peso_silo_diario`
  MODIFY `id` int(6) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `plantas`
--
ALTER TABLE `plantas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `proceso`
--
ALTER TABLE `proceso`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `region`
--
ALTER TABLE `region`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT de la tabla `registro_cerdos`
--
ALTER TABLE `registro_cerdos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `registro_mortalidad`
--
ALTER TABLE `registro_mortalidad`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT de la tabla `sector`
--
ALTER TABLE `sector`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `silo`
--
ALTER TABLE `silo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=135;

--
-- AUTO_INCREMENT de la tabla `temperatura_cerdos`
--
ALTER TABLE `temperatura_cerdos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `temperatura_deseada`
--
ALTER TABLE `temperatura_deseada`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `tipo_instalacion`
--
ALTER TABLE `tipo_instalacion`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `weather_underground`
--
ALTER TABLE `weather_underground`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `zona`
--
ALTER TABLE `zona`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `area_productiva`
--
ALTER TABLE `area_productiva`
  ADD CONSTRAINT `area_productiva_proceso_id_fk` FOREIGN KEY (`id_proceso`) REFERENCES `proceso` (`id`);

--
-- Filtros para la tabla `bienestar_equipo`
--
ALTER TABLE `bienestar_equipo`
  ADD CONSTRAINT `bienestar_equipo_equipos_id_fk` FOREIGN KEY (`id_equipo`) REFERENCES `equipos` (`id`);

--
-- Filtros para la tabla `crianza`
--
ALTER TABLE `crianza`
  ADD CONSTRAINT `crianza__id_fk` FOREIGN KEY (`id_sector`) REFERENCES `sector` (`id`);

--
-- Filtros para la tabla `crianza_pabellon`
--
ALTER TABLE `crianza_pabellon`
  ADD CONSTRAINT `crianza_pabellon___fk` FOREIGN KEY (`id_pabellon`) REFERENCES `pabellon` (`id`);

--
-- Filtros para la tabla `detalle_dieta_sectores`
--
ALTER TABLE `detalle_dieta_sectores`
  ADD CONSTRAINT `detalle_dieta_sectores___fk` FOREIGN KEY (`id_sector`) REFERENCES `sector` (`id`),
  ADD CONSTRAINT `detalle_dieta_sectores___fk_2` FOREIGN KEY (`id_dieta`) REFERENCES `dieta` (`id`);

--
-- Filtros para la tabla `dieta_historico`
--
ALTER TABLE `dieta_historico`
  ADD CONSTRAINT `dieta_historico___fk` FOREIGN KEY (`id_guia`) REFERENCES `guias_despacho` (`id`),
  ADD CONSTRAINT `dieta_historico_dieta_id_fk` FOREIGN KEY (`id_dieta`) REFERENCES `dieta` (`id`);

--
-- Filtros para la tabla `equipos`
--
ALTER TABLE `equipos`
  ADD CONSTRAINT `equipos_equipo_tipo_id_fk` FOREIGN KEY (`id_tipo_equipo`) REFERENCES `equipo_tipo` (`id`),
  ADD CONSTRAINT `equipos_pabellon_id_fk` FOREIGN KEY (`id_pabellon`) REFERENCES `pabellon` (`id`);

--
-- Filtros para la tabla `guias_despacho`
--
ALTER TABLE `guias_despacho`
  ADD CONSTRAINT `dieta_fk` FOREIGN KEY (`dieta_id`) REFERENCES `dieta` (`id`),
  ADD CONSTRAINT `sectors_fk` FOREIGN KEY (`sector_id`) REFERENCES `sector` (`id`);

--
-- Filtros para la tabla `kpi_diarios`
--
ALTER TABLE `kpi_diarios`
  ADD CONSTRAINT `kpi_diarios2_pabellon_id_fk` FOREIGN KEY (`id_pabellon`) REFERENCES `pabellon` (`id`);

--
-- Filtros para la tabla `macrozona`
--
ALTER TABLE `macrozona`
  ADD CONSTRAINT `macrozona_region_id_fk` FOREIGN KEY (`id_region`) REFERENCES `region` (`id`);

--
-- Filtros para la tabla `medicion_pabellon`
--
ALTER TABLE `medicion_pabellon`
  ADD CONSTRAINT `medicion_pabellon_equipos_id_fk` FOREIGN KEY (`id_equipo`) REFERENCES `equipos` (`id`);

--
-- Filtros para la tabla `pabellon`
--
ALTER TABLE `pabellon`
  ADD CONSTRAINT `pabellon_sector___fk` FOREIGN KEY (`id_sector`) REFERENCES `sector` (`id`);

--
-- Filtros para la tabla `plantas`
--
ALTER TABLE `plantas`
  ADD CONSTRAINT `plantas_area_productiva_id_fk` FOREIGN KEY (`id_area_productiva`) REFERENCES `area_productiva` (`id`);

--
-- Filtros para la tabla `proceso`
--
ALTER TABLE `proceso`
  ADD CONSTRAINT `proceso_tipo_instalacion_id_fk` FOREIGN KEY (`id_instalacion`) REFERENCES `tipo_instalacion` (`id`);

--
-- Filtros para la tabla `region`
--
ALTER TABLE `region`
  ADD CONSTRAINT `region_pais___fk` FOREIGN KEY (`id_pais`) REFERENCES `pais` (`id`);

--
-- Filtros para la tabla `registro_mortalidad`
--
ALTER TABLE `registro_mortalidad`
  ADD CONSTRAINT `registro_mortalidad___fk` FOREIGN KEY (`pabellon_id`) REFERENCES `pabellon` (`id`),
  ADD CONSTRAINT `registro_mortalidad___fk_2` FOREIGN KEY (`causa_id`) REFERENCES `mortalidad` (`id`);

--
-- Filtros para la tabla `sector`
--
ALTER TABLE `sector`
  ADD CONSTRAINT `sector___fk` FOREIGN KEY (`username`) REFERENCES `usuario` (`username`),
  ADD CONSTRAINT `sector_zona__fk` FOREIGN KEY (`id_zona`) REFERENCES `zona` (`id`);

--
-- Filtros para la tabla `silo`
--
ALTER TABLE `silo`
  ADD CONSTRAINT `silo_pabellon_id_fk` FOREIGN KEY (`id_pabellon`) REFERENCES `pabellon` (`id`);

--
-- Filtros para la tabla `zona`
--
ALTER TABLE `zona`
  ADD CONSTRAINT `zona_macrozona___fk` FOREIGN KEY (`id_macrozona`) REFERENCES `macrozona` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
